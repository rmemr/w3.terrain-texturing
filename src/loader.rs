// ----------------------------------------------------------------------------
use std::fs::File;
use std::path::{Path, PathBuf};

use bevy::prelude::*;

use futures_lite::Future;

use png::{BitDepth, ColorType};

use crate::project::TextureMaps;

use crate::heightmap::TerrainHeightMap;
use crate::texturecontrol::TextureControl as TextureControlMap;
use crate::tintmap::TintMap;
use crate::TaskResultData;
// ----------------------------------------------------------------------------
pub struct LoaderPlugin;
// ----------------------------------------------------------------------------
#[derive(Hash, PartialEq, Eq, Clone, Copy)]
pub enum MapType {
    Heightmap,
    Texturing,
    TextureControl,
    Tintmap,
}
// ----------------------------------------------------------------------------
#[derive(Clone)]
pub enum DataMapping<T> {
    None,
    File(PathBuf),
    Const(T),
}
// ----------------------------------------------------------------------------
impl LoaderPlugin {
    // ----------------------------------------------------------------------------
    pub fn check_img_file(
        path: &Path,
        expected_width: u32,
        expected_height: u32,
        color_type: ColorType,
        bitdepth: BitDepth,
        palette_size: Option<usize>,
    ) -> Result<(), String> {
        let decoder = png::Decoder::new(
            File::open(path)
                .map_err(|e| format!("failed to open file {}: {}", path.display(), e))?,
        );
        let reader = decoder
            .read_info()
            .map_err(|e| format!("failed to read image info: {}", e))?;
        let info = reader.info();

        if expected_width != info.width || expected_height != info.height {
            Err(format!(
                "expected image size {}x{}. found: {}x{}",
                expected_width, expected_height, info.width, info.height
            ))
        } else if color_type != info.color_type {
            Err(format!(
                "expected image format {:?}. found: {:?}",
                color_type, info.color_type
            ))
        } else if bitdepth != info.bit_depth {
            Err(format!(
                "expected image bit depth {:?}. found: {:?}",
                bitdepth, info.bit_depth
            ))
        } else if let Some(palette) = info.palette.as_ref() {
            if palette.len() != palette_size.unwrap_or(0) * 3 {
                Err(format!(
                    "expected image palette size {:?}. found: {:?}",
                    palette_size,
                    palette.len() / 3
                ))
            } else {
                Ok(())
            }
        } else {
            Ok(())
        }
    }
    // ------------------------------------------------------------------------
    pub(crate) async fn load_heightmap(
        basedir: PathBuf,
        source: DataMapping<u16>,
        size: u32,
        height_scaling: f32,
    ) -> Result<TaskResultData, String> {
        use byteorder::{BigEndian, ReadBytesExt};
        use png::{BitDepth::Sixteen, ColorType::Grayscale};
        use std::io::Cursor;

        let data = match source {
            DataMapping::None => {
                warn!("generating placeholder heightmap...");
                generate_placeholder_heightmap(size)
            }
            DataMapping::File(filepath) => {
                debug!("loading heightmap...");
                let img_data =
                    Self::load_png_data(Grayscale, Sixteen, size, &basedir.join(filepath))?;

                // transform buffer into 16 bits
                let mut buffer_u16 = vec![0; (size * size) as usize];
                let mut buffer_cursor = Cursor::new(img_data);
                buffer_cursor
                    .read_u16_into::<BigEndian>(&mut buffer_u16)
                    .map_err(|e| format!("failed to convert buffer into u16 values: {}", e))?;

                buffer_u16
            }
            DataMapping::Const(value) => {
                debug!("generating constant value heightmap...");
                vec![value; (size * size) as usize]
            }
        };

        let heightmap = TerrainHeightMap::new(size, height_scaling, data);
        Ok(TaskResultData::HeightmapData(heightmap))
    }
    // ------------------------------------------------------------------------
    pub(crate) fn load_texturemap(
        basedir: PathBuf,
        texturing: &TextureMaps,
        size: u32,
    ) -> impl Future<Output = Result<TaskResultData, String>> {
        use MapType::*;

        let (background, overlay, blendcontrol) = (
            texturing.background().to_owned(),
            texturing.overlay().to_owned(),
            texturing.blendcontrol().to_owned(),
        );
        async move {
            use DataMapping::*;
            let load_map = |name, maptype, filepath| -> Result<Vec<u8>, String> {
                debug!("loading {} texturing map...", name);
                Self::load_texturing_data(
                    maptype,
                    size,
                    &format!("{} texture id", name),
                    &basedir.join(filepath),
                )
            };
            let generate_placeholder_map = |name| -> Vec<u8> {
                warn!("generating placeholder value {} texturing...", name);
                vec![1u8; (size * size) as usize]
            };
            let generate_const_map = |name, value| -> Vec<u8> {
                debug!("generating constant value {} texturing...", name);
                vec![value; (size * size) as usize]
            };

            let background = match background {
                None => generate_placeholder_map("background"),
                File(filepath) => load_map("background", Texturing, filepath)?,
                Const(v) => generate_const_map("background", v),
            };

            let overlay = match overlay {
                None => generate_placeholder_map("overlay"),
                File(filepath) => load_map("overlay", Texturing, filepath)?,
                Const(v) => generate_const_map("overlay", v),
            };

            let blendcontrol = match blendcontrol {
                None => generate_placeholder_map("blendcontrol"),
                File(filepath) => load_map("blendcontrol", TextureControl, filepath)?,
                Const(v) => generate_const_map("blendcontrol", v),
            };

            if overlay.len() != background.len() || blendcontrol.len() != background.len() {
                return Err("size of texture maps is not equal!".to_string());
            }

            let controlmap = background
                .iter()
                .zip(overlay.iter())
                .zip(blendcontrol.iter())
                .map(|((background, overlay), blendcontrol)| {
                    // 0..4 overlay texture idx
                    // 5..9 background textures idx
                    // 10..15 blend control
                    //   10..12 slope threshold
                    //   13..15 UV scale
                    u16::from(*overlay)
                        + (u16::from(*background) << 5)
                        + (u16::from(*blendcontrol) << 10)
                })
                .collect::<Vec<u16>>();

            Ok(TaskResultData::TextureControl(TextureControlMap::new(
                size, controlmap,
            )))
        }
    }
    // ------------------------------------------------------------------------
    pub(crate) async fn load_tintmap(
        basedir: PathBuf,
        source: DataMapping<u32>,
        size: u32,
    ) -> Result<TaskResultData, String> {
        let tintmap = match source {
            DataMapping::None => {
                warn!("generating neutral color tintmap...");
                vec![0x80u8; 4 * (size * size) as usize]
            }
            DataMapping::File(filepath) => {
                debug!("loading tintmap...");
                let (colortype, bitdepth, _) = MapType::Tintmap.properties();
                Self::load_png_data(colortype, bitdepth, size, &basedir.join(filepath))?
            }
            DataMapping::Const(color) => {
                use byteorder::{LittleEndian, WriteBytesExt};

                debug!("generating constant color tintmap...");
                let mut colors = Vec::with_capacity((size * size * 4) as usize);

                for _ in 0..size * size {
                    colors
                        .write_u32::<LittleEndian>(color)
                        .map_err(|e| e.to_string())?;
                }
                colors
            }
        };
        Ok(TaskResultData::TintMap(TintMap::new(size, tintmap)))
    }
    // ------------------------------------------------------------------------
    pub(crate) async fn load_terrain_texture(
        filepath: PathBuf,
        size: u32,
    ) -> Result<image::RgbaImage, String> {
        use png::{BitDepth::Eight, ColorType::Rgba};
        let data = Self::load_png_data(Rgba, Eight, size, &filepath)?;
        Ok(image::RgbaImage::from_raw(size, size, data).unwrap())
    }
    // ------------------------------------------------------------------------
    pub fn load_png_data(
        colortype: ColorType,
        bitdepth: BitDepth,
        resolution: u32,
        filepath: &Path,
    ) -> Result<Vec<u8>, String> {
        use png::{Decoder, Transformations};

        let file = File::open(filepath)
            .map_err(|e| format!("failed to open file {}: {}", filepath.display(), e))?;

        let mut decoder = Decoder::new(file);
        decoder.set_transformations(Transformations::IDENTITY);

        let mut reader = decoder
            .read_info()
            .map_err(|e| format!("failed to decode png file {}: {}", filepath.display(), e))?;

        let mut img_data = vec![0; reader.output_buffer_size()];
        let info = reader.next_frame(&mut img_data).map_err(|e| {
            format!(
                "failed to read image format info for: {}: {}",
                filepath.display(),
                e
            )
        })?;

        if info.color_type != colortype || info.bit_depth != bitdepth {
            return Err(format!(
                "file {}: format must be {:?}-Bit {:?}. found {:?}-Bit {:?}",
                filepath.display(),
                bitdepth,
                colortype,
                info.bit_depth,
                info.color_type
            ));
        }
        if info.width != resolution || info.height != resolution {
            return Err(format!(
                "file {}: expected width x height to be {} x {}. found: {} x {}",
                filepath.display(),
                resolution,
                resolution,
                info.width,
                info.height
            ));
        }

        Ok(img_data)
    }
    // ------------------------------------------------------------------------
    fn load_texturing_data(
        maptype: MapType,
        resolution: u32,
        dataname: &str,
        filepath: &Path,
    ) -> Result<Vec<u8>, String> {
        let (colortype, bitdepth, colors) = maptype.properties();
        let max_color_idx = (colors.unwrap_or(1) - 1) as u8;

        let img_data = Self::load_png_data(colortype, bitdepth, resolution, filepath)?;

        // check value range
        for (i, pix) in img_data.iter().enumerate() {
            if *pix > max_color_idx {
                let y = i / resolution as usize;
                let x = i - y * resolution as usize;
                return Err(format!(
                    "valid range for {} is [0..{}]. found: {} at line {} in pixel {}",
                    dataname,
                    max_color_idx,
                    pix,
                    y + 1,
                    x + 1
                ));
            }
        }
        Ok(img_data)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl MapType {
    pub const fn properties(&self) -> (png::ColorType, png::BitDepth, Option<usize>) {
        use png::BitDepth::*;
        use png::ColorType::*;

        match self {
            MapType::Heightmap => (Grayscale, Sixteen, None),
            MapType::Texturing => (Indexed, Eight, Some(32)),
            MapType::TextureControl => (Indexed, Eight, Some(64)),
            MapType::Tintmap => (Rgba, Eight, None),
        }
    }
}
// ----------------------------------------------------------------------------
fn generate_placeholder_heightmap(gen_size: u32) -> Vec<u16> {
    let mut generated_heightmap = Vec::with_capacity((gen_size * gen_size) as usize);
    for y in 0..gen_size {
        for x in 0..gen_size {
            let scale = 7.0 / gen_size as f32 * (gen_size as f32 / 256.0);
            let x = x as f32;
            let y = y as f32;
            let v = 1.0 + (scale * (x + 0.76 * y)).sin() * (scale * y / 2.0).cos();

            generated_heightmap.push(((u16::MAX / 4) as f32 * v) as u16);
        }
    }
    generated_heightmap
}
// ----------------------------------------------------------------------------
impl<T> Default for DataMapping<T> {
    fn default() -> Self {
        Self::None
    }
}
// ----------------------------------------------------------------------------
impl<T> DataMapping<T> {
    // ------------------------------------------------------------------------
    pub fn as_path(&self) -> Result<&Path, String> {
        if let DataMapping::File(path) = self {
            Ok(path)
        } else {
            Err("data mapping type is not FILE".to_string())
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
