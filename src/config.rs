// ----------------------------------------------------------------------------
// 256 seems to be a good compromise for 16k x 16k terrains.
pub const TILE_SIZE: u32 = 256;
// Since a clipmap level is assigned to a tile which is completely covered by
// clipmap level the clipmap size should be at least 4 * TILE_SIZE to make sure
// that at least 3 tiles (>= 1.5 tiles in all directions from camera) are
// highest res. Good value is 1024.
// Note: atm MAX is 1024 since this amount of rays are used to compute shadows
pub const CLIPMAP_SIZE: u32 = TILE_SIZE * 4;
// Granularity of clipmap view positions in full data. Since clipmap levels are
// assigned to tiles this should be the same size (lower granularity will update
// clipmap data more often but tiles will only be updated if they are fully
// covered).
pub const CLIPMAP_GRANULARITY: u32 = TILE_SIZE;
// ----------------------------------------------------------------------------
/// config for current world/terrain
#[derive(Clone)]
pub struct TerrainConfig {
    /// terrain size in meters
    terrain_size: f32,
    /// pixel size of all maps
    map_size: u32,
    /// precalculated resolution of terrain (terrain_size / map_size)
    resolution: f32,
    /// lowest height of terrain in meters (absolute)
    min_height: f32,
    /// heighest height of terrain in meters (absolute)
    max_height: f32,
    /// clipmnap levels
    clipmap_levels: u8,
}
// ----------------------------------------------------------------------------
use bevy::math::{uvec2, vec2, UVec2, Vec2};
// ----------------------------------------------------------------------------
impl TerrainConfig {
    // ------------------------------------------------------------------------
    pub fn new(
        map_size: u32,
        terrain_size: f32,
        min_height: f32,
        max_height: f32,
        clipmap_levels: u8,
    ) -> Self {
        let resolution = terrain_size / map_size as f32;
        TerrainConfig {
            terrain_size,
            map_size,
            resolution,
            min_height,
            max_height,
            clipmap_levels,
        }
    }
    // ------------------------------------------------------------------------
    /// resolution m/px
    #[inline(always)]
    pub fn resolution(&self) -> f32 {
        self.resolution
    }
    // ------------------------------------------------------------------------
    #[allow(dead_code)]
    #[inline(always)]
    pub fn terrain_size(&self) -> f32 {
        self.terrain_size
    }
    // ------------------------------------------------------------------------
    #[inline(always)]
    pub fn map_size(&self) -> u32 {
        self.map_size
    }
    // ------------------------------------------------------------------------
    #[inline(always)]
    pub fn map_offset(&self) -> Vec2 {
        // assumption is: map is centered around origin, 4 tile corners at origin
        let tiles = self.map_size / TILE_SIZE;
        let tile_offset = (tiles / 2) as f32 * TILE_SIZE as f32;

        vec2(-tile_offset, -tile_offset) * self.resolution
    }
    // ------------------------------------------------------------------------
    #[inline(always)]
    pub fn min_height(&self) -> f32 {
        self.min_height
    }
    // ------------------------------------------------------------------------
    #[inline(always)]
    pub fn max_height(&self) -> f32 {
        self.max_height
    }
    // ------------------------------------------------------------------------
    #[inline(always)]
    pub fn height_scaling(&self) -> f32 {
        (self.max_height - self.min_height) / u16::MAX as f32
    }
    // ------------------------------------------------------------------------
    #[inline(always)]
    pub fn tiles_per_edge(&self) -> u8 {
        (self.map_size / TILE_SIZE) as u8
    }
    // ------------------------------------------------------------------------
    #[inline(always)]
    pub fn tile_count(&self) -> usize {
        (self.map_size / TILE_SIZE * self.map_size / TILE_SIZE) as usize
    }
    // ------------------------------------------------------------------------
    #[inline(always)]
    pub fn clipmap_levels(&self) -> u8 {
        self.clipmap_levels
    }
    // ------------------------------------------------------------------------
    #[inline(always)]
    pub fn max_clipmap_level(&self) -> u8 {
        self.clipmap_levels - 1
    }
    // ------------------------------------------------------------------------
    #[inline(always)]
    pub fn world_pos_to_map_pos(&self, pos: Vec2) -> UVec2 {
        let map_offset = self.map_offset();

        ((pos - map_offset) / self.resolution)
            .round()
            .as_uvec2()
            // clamp to data size
            .min(uvec2(self.map_size - 1, self.map_size - 1))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// defaults
// ----------------------------------------------------------------------------
impl Default for TerrainConfig {
    // ------------------------------------------------------------------------
    fn default() -> Self {
        Self::new(1024, 512.0, -37.0, 45.0, 3)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
