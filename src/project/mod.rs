// ----------------------------------------------------------------------------
use std::path::{Path, PathBuf};

use bevy::ecs::schedule::StateData;
use bevy::prelude::*;
use bevy::tasks::Task;
use bevy::utils::HashMap;

use crate::config;
use crate::loader::{DataMapping, LoaderPlugin, MapType};
// ----------------------------------------------------------------------------
/// config for texturing maps
#[derive(Clone, Default)]
pub struct TextureMaps {
    // stackvalue := background texture id + overlay textureid + blendcontrol
    background: DataMapping<u8>,
    overlay: DataMapping<u8>,
    blendcontrol: DataMapping<u8>,
}
// ----------------------------------------------------------------------------
#[derive(Clone, Default)]
struct ModifiedState {
    definition: bool,
    texturing: bool,
    heightmap: bool,
    tintmap: bool,
}
// ----------------------------------------------------------------------------
/// settings for current project
#[derive(Clone)]
pub struct ProjectSettings {
    name: String,
    /// all definitions + maps related to this project must located in this
    /// directory
    project_path: PathBuf,
    /// filename (without path) for world definition
    world_definition_file: PathBuf,
    /// terrain dimension / resolution config
    terrain: config::TerrainConfig,
    /// path to heightmap
    heightmap: DataMapping<u16>,
    /// paths to texture and blend control maps
    texturemaps: TextureMaps,
    /// path to tint/pigment/color map
    tintmap: DataMapping<u32>,
    /// currently assigned materialset info
    materialset: MaterialSetConfig,
    /// assigned environment definition
    environment: Option<String>,
    /// contains error msgs if a setting is deemed invalid
    validation: HashMap<ValidatedSetting, String>,
    /// additional data from definition file is kept after loading so it can be
    /// stored on saving
    unused_definitions: HashMap<&'static str, definitions::UnusedDefinitionData>,
    /// modified state
    modified: ModifiedState,
    /// flag to prevent quick saving (provided example project should be saved
    /// in different directory first)
    no_quicksave: bool,
}
// ----------------------------------------------------------------------------
#[derive(PartialEq, Eq, Hash, Clone, Copy)]
pub enum ValidatedSetting {
    WorldId,
    Heightmap,
    Tintmap,
    TexturingBackground,
    TexturingOverlay,
    TexturingControl,
    TerrainSize,
    TerrainHeight,
    MapSize,
}
// ----------------------------------------------------------------------------
pub use self::backup::backup_project;
pub use self::definitions::load_def;
pub use self::definitions::save_def;
pub use self::materialset::{
    MaterialSetConfig, MaterialSetRegistry, MaterialSlot, TerrainMaterialParam,
};
// ----------------------------------------------------------------------------
pub struct ProjectSettingsPlugin;
// ----------------------------------------------------------------------------
impl ProjectSettingsPlugin {
    // ------------------------------------------------------------------------
    #[allow(clippy::let_and_return)]
    pub fn startup() -> SystemSet {
        let set = SystemSet::new().with_system(materialset::setup_materialset_registry);

        #[cfg(feature = "witcher3")]
        let set = set.with_system(w3plugin::setup_materialset_registry);

        set
    }
    // ------------------------------------------------------------------------
    pub fn setup_saving<T: StateData>(state: T) -> Vec<SystemSet> {
        vec![
            SystemSet::on_enter(state.clone())
                .with_system(systems::setup_saving.chain(systems::handle_save_error)),
            SystemSet::on_update(state)
                .with_system(systems::start_async_store_map_tasks.chain(systems::handle_save_error))
                .with_system(systems::poll_async_store_map_tasks.chain(systems::handle_save_error))
                .with_system(systems::watch_saving),
        ]
    }
    // ------------------------------------------------------------------------
    pub fn watch_editing_operations<T: StateData>(state: T) -> SystemSet {
        SystemSet::on_update(state).with_system(systems::watch_editing)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl Plugin for ProjectSettingsPlugin {
    // ------------------------------------------------------------------------
    fn build(&self, app: &mut App) {
        app.init_resource::<ProjectSettings>()
            .init_resource::<MaterialSetRegistry>()
            .init_resource::<AsyncTaskProgress>();
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
type Result<T> = std::result::Result<T, std::string::String>;
// ----------------------------------------------------------------------------
#[derive(Component)]
struct StoreMapTask(MapType, Task<Result<PathBuf>>);
// ----------------------------------------------------------------------------
#[derive(Default)]
struct AsyncTaskProgress(HashMap<MapType, (usize, usize)>);
// ----------------------------------------------------------------------------
mod backup;
mod definitions;
mod mapwriter;
mod materialset;
mod systems;

#[cfg(feature = "witcher3")]
mod w3plugin;
// ----------------------------------------------------------------------------
impl ProjectSettings {
    // ------------------------------------------------------------------------
    #[allow(clippy::too_many_arguments)]
    pub fn try_new(
        name: &str,
        world_definition: PathBuf,
        map_size: u32,
        terrain_size: f32,
        min_height: f32,
        max_height: f32,
        heightmap: DataMapping<u16>,
        tintmap: DataMapping<u32>,
        texturemaps: TextureMaps,
        materialset: MaterialSetConfig,
        environment: Option<String>,
        clipmap_levels: u8,
    ) -> Result<Self> {
        if world_definition.is_file() {
            Ok(Self::new(
                name,
                world_definition,
                map_size,
                terrain_size,
                min_height,
                max_height,
                heightmap,
                tintmap,
                texturemaps,
                materialset,
                environment,
                clipmap_levels,
            ))
        } else {
            Err(format!(
                "world definition [{}] file not found.",
                world_definition.display()
            ))
        }
    }
    // ------------------------------------------------------------------------
    #[allow(clippy::too_many_arguments)]
    pub fn new(
        name: &str,
        world_definition: PathBuf,
        map_size: u32,
        terrain_size: f32,
        min_height: f32,
        max_height: f32,
        heightmap: DataMapping<u16>,
        tintmap: DataMapping<u32>,
        texturemaps: TextureMaps,
        materialset: MaterialSetConfig,
        environment: Option<String>,
        clipmap_levels: u8,
    ) -> Self {
        // Note: this implies world_definition pathbuf points to a file
        let world_definition_file = world_definition
            .file_name()
            .expect("world definition filename")
            .to_os_string()
            .into();

        let project_path = world_definition
            .parent()
            .expect("parent path for world definition file");

        Self {
            name: name.to_string(),
            project_path: project_path.to_path_buf(),
            world_definition_file,
            terrain: config::TerrainConfig::new(
                map_size,
                terrain_size,
                min_height,
                max_height,
                clipmap_levels,
            ),
            heightmap,
            texturemaps,
            tintmap,
            materialset,
            environment,
            validation: HashMap::default(),
            unused_definitions: HashMap::default(),
            modified: ModifiedState::default(),
            no_quicksave: false,
        }
    }
    // ------------------------------------------------------------------------
    #[inline(always)]
    pub fn name(&self) -> &str {
        &self.name
    }
    // ------------------------------------------------------------------------
    #[inline(always)]
    pub fn project_directory(&self) -> &Path {
        &self.project_path
    }
    // ------------------------------------------------------------------------
    #[inline(always)]
    pub fn world_definition_file(&self) -> &Path {
        &self.world_definition_file
    }
    // ------------------------------------------------------------------------
    #[inline(always)]
    pub fn terrain_config(&self) -> &config::TerrainConfig {
        &self.terrain
    }
    // ------------------------------------------------------------------------
    #[inline(always)]
    pub fn heightmap(&self) -> &DataMapping<u16> {
        &self.heightmap
    }
    // ------------------------------------------------------------------------
    #[inline(always)]
    pub fn tintmap(&self) -> &DataMapping<u32> {
        &self.tintmap
    }
    // ------------------------------------------------------------------------
    #[inline(always)]
    pub fn texturemaps(&self) -> &TextureMaps {
        &self.texturemaps
    }
    // ------------------------------------------------------------------------
    #[inline(always)]
    pub fn materialset(&self) -> &MaterialSetConfig {
        &self.materialset
    }
    // ------------------------------------------------------------------------
    #[inline(always)]
    pub fn environment_definition(&self) -> Option<&str> {
        self.environment.as_deref()
    }
    // ------------------------------------------------------------------------
    #[inline]
    pub fn is_valid(&self) -> bool {
        self.validation.is_empty()
    }
    // ------------------------------------------------------------------------
    #[inline]
    pub fn no_quicksave(&self) -> bool {
        self.no_quicksave
    }
    // ------------------------------------------------------------------------
    #[inline]
    pub fn prevent_quicksave(&mut self) {
        self.no_quicksave = true;
    }
    // ------------------------------------------------------------------------
    #[inline]
    pub fn is_modified(&self) -> bool {
        self.modified.definition
            || self.modified.texturing
            || self.modified.tintmap
            || self.modified.heightmap
    }
    // ------------------------------------------------------------------------
    #[inline]
    pub fn set_modified_texturing(&mut self) {
        self.modified.texturing = true;
    }
    // ------------------------------------------------------------------------
    #[inline]
    pub fn set_all_modified(&mut self) {
        self.modified.definition = true;
        self.modified.texturing = true;
        self.modified.tintmap = true;
        self.modified.heightmap = true;
    }
    // ------------------------------------------------------------------------
    pub fn reset_modified(&mut self) {
        self.modified = ModifiedState::default();
    }
    // ------------------------------------------------------------------------
    pub fn validation_error(&self, setting: ValidatedSetting) -> Option<&String> {
        self.validation.get(&setting)
    }
    // ------------------------------------------------------------------------
    pub fn update_material_parameter(
        &mut self,
        slot: MaterialSlot,
        new_params: TerrainMaterialParam,
    ) {
        self.materialset.set_parameter(slot, new_params);
    }
    // ------------------------------------------------------------------------
    pub fn setup_maps_as_files(&mut self) {
        use DataMapping::*;

        // setup default filenames
        if let File(_) = self.heightmap {
        } else {
            self.heightmap = File(PathBuf::from("heightmap.png"));
        }
        if let File(_) = self.tintmap {
        } else {
            self.tintmap = File(PathBuf::from("tintmap.png"));
        }

        self.texturemaps.setup_as_files();
    }
    // ------------------------------------------------------------------------
    pub fn set_project_directory(&mut self, dir: &Path) {
        if dir.is_dir() {
            self.project_path = dir.to_path_buf();
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl TextureMaps {
    // ------------------------------------------------------------------------
    pub fn new(
        background: DataMapping<u8>,
        overlay: DataMapping<u8>,
        blendcontrol: DataMapping<u8>,
    ) -> Self {
        Self {
            background,
            overlay,
            blendcontrol,
        }
    }
    // ------------------------------------------------------------------------
    #[inline(always)]
    pub fn background(&self) -> &DataMapping<u8> {
        &self.background
    }
    // ------------------------------------------------------------------------
    #[inline(always)]
    pub fn overlay(&self) -> &DataMapping<u8> {
        &self.overlay
    }
    // ------------------------------------------------------------------------
    #[inline(always)]
    pub fn blendcontrol(&self) -> &DataMapping<u8> {
        &self.blendcontrol
    }
    // ------------------------------------------------------------------------
    fn setup_as_files(&mut self) {
        use DataMapping::*;

        // setup default filenames
        if let File(_) = self.background {
        } else {
            self.background = File(PathBuf::from("background.png"));
        }

        if let File(_) = self.overlay {
        } else {
            self.overlay = File(PathBuf::from("overlay.png"));
        }

        if let File(_) = self.blendcontrol {
        } else {
            self.blendcontrol = File(PathBuf::from("blendcontrol.png"));
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// validation
// ----------------------------------------------------------------------------
impl ProjectSettings {
    // ------------------------------------------------------------------------
    pub fn validate(&mut self, _id: &str) -> Result<()> {
        use ValidatedSetting::*;

        self.validation.clear();

        if self.name.is_empty() || self.name.len() < 3 {
            self.validation
                .insert(WorldId, format!("invalid world name {}", self.name));
        }

        if self.terrain.terrain_size() < 1000.0 || self.terrain.terrain_size() > 10000.0 {
            self.validation.insert(
                TerrainSize,
                format!(
                    "supported terrainsize is [1000..10000]. found: {}",
                    self.terrain.terrain_size()
                ),
            );
        }

        if ![1024, 2048, 4096, 8192, 16384].contains(&self.terrain.map_size()) {
            self.validation.insert(
                MapSize,
                format!(
                    "supported map sizes are 1024, 2048, 4096, 8192 or 16384. found: {}",
                    self.terrain.map_size()
                ),
            );
        }

        if self.terrain.min_height() > self.terrain.max_height() {
            self.validation.insert(
                TerrainHeight,
                format!(
                    "terrain height minimum must be lower than height maximum. found: {} > {}",
                    self.terrain.min_height(),
                    self.terrain.max_height()
                ),
            );
        }

        if self.terrain.min_height() < -10000.0 || self.terrain.max_height() > 10000.0 {
            self.validation.insert(
                TerrainHeight,
                format!(
                    "terrain height must be between -10000 and +10000. found: ({} {})",
                    self.terrain.min_height(),
                    self.terrain.max_height()
                ),
            );
        }

        fn check_mapfile<T: std::fmt::Debug>(
            maptype: MapType,
            base_path: &Path,
            data: &DataMapping<T>,
            expected_size: u32,
        ) -> Result<()> {
            use DataMapping::*;

            match data {
                File(filename) => {
                    let (color_type, bitdepth, palette_size) = maptype.properties();
                    let path = base_path.join(filename);
                    if path.is_file() {
                        LoaderPlugin::check_img_file(
                            &path,
                            expected_size,
                            expected_size,
                            color_type,
                            bitdepth,
                            palette_size,
                        )
                    } else {
                        Err("file not found.".to_string())
                    }
                }
                _ => Ok(()),
            }
        }

        // -- check files
        use MapType::*;
        let size = self.terrain.map_size();
        let texturing = &self.texturemaps;
        let basedir = &self.project_path;

        if let Err(err) = check_mapfile(MapType::Heightmap, basedir, &self.heightmap, size) {
            self.validation.insert(ValidatedSetting::Heightmap, err);
        }
        if let Err(err) = check_mapfile(MapType::Tintmap, basedir, &self.tintmap, size) {
            self.validation.insert(ValidatedSetting::Tintmap, err);
        }
        if let Err(err) = check_mapfile(Texturing, basedir, &texturing.background, size) {
            self.validation.insert(TexturingBackground, err);
        }
        if let Err(err) = check_mapfile(Texturing, basedir, &texturing.overlay, size) {
            self.validation.insert(TexturingOverlay, err);
        }
        if let Err(err) = check_mapfile(TextureControl, basedir, &texturing.blendcontrol, size) {
            self.validation.insert(TexturingControl, err);
        }

        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// defaults
// ----------------------------------------------------------------------------
impl Default for ProjectSettings {
    // ------------------------------------------------------------------------
    fn default() -> Self {
        Self::test_terrain(1024)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// fmt
// ----------------------------------------------------------------------------
use std::fmt;

impl fmt::Debug for ProjectSettings {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "ProjectSettings: {}", self.name())
    }
}
// ----------------------------------------------------------------------------
impl ProjectSettings {
    // ------------------------------------------------------------------------
    #[allow(dead_code)]
    pub fn test_terrain(size: u32) -> Self {
        use DataMapping::File;
        let basepath = "_test-data_/terrain/";
        Self {
            name: format!("Test Terrain ({} x {})", size, size),
            terrain: config::TerrainConfig::new(size, size as f32 / 2.0, -37.0, 45.0, 3),
            heightmap: File(Self::generate_map_filename("heightmap", size)),
            texturemaps: TextureMaps {
                background: File(Self::generate_map_filename("bkgrnd", size)),
                overlay: File(Self::generate_map_filename("overlay", size)),
                blendcontrol: File(Self::generate_map_filename("blendcontrol", size)),
            },
            tintmap: File(Self::generate_map_filename("tint", size)),
            materialset: MaterialSetConfig::test_terrain(),
            environment: Some(
                "environment/definitions/env_prologue/env_prolog_colors_v1_b_sunset.env"
                    .to_string(),
            ),
            project_path: PathBuf::from(basepath),
            world_definition_file: PathBuf::from("Test Terrain"),
            validation: HashMap::default(),
            unused_definitions: HashMap::default(),
            modified: ModifiedState::default(),
            no_quicksave: true,
        }
    }
    // ------------------------------------------------------------------------
    fn generate_map_filename(mapname: &str, size: u32) -> PathBuf {
        PathBuf::from(format!("test.{}.{}x{}.png", mapname, size, size))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<T: std::fmt::Display + std::fmt::Debug> DataMapping<T> {
    // ------------------------------------------------------------------------
    pub fn display(&self) -> String {
        match self {
            DataMapping::None => "- placeholder -".to_string(),
            DataMapping::File(f) => shorten::<50>(f.display().to_string()),
            DataMapping::Const(v) => format!("const value: {}", v),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
fn shorten<const LEN: usize>(s: String) -> String {
    if s.len() > LEN {
        format!("...{}", s.split_at(s.len() - LEN).1)
    } else {
        s
    }
}
// ----------------------------------------------------------------------------
