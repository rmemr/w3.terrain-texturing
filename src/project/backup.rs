// ----------------------------------------------------------------------------
pub fn backup_project(
    path: &Path,
    settings: &ProjectSettings,
    full_backup: bool,
) -> Result<(), String> {
    debug!("> creating backup of current quest definition files...");

    let mut filelist = vec![path.join(settings.world_definition_file())];
    // texturing maps are always included
    for data in [
        settings.texturemaps().background(),
        settings.texturemaps().overlay(),
        settings.texturemaps().blendcontrol(),
    ] {
        if let DataMapping::File(file) = data {
            filelist.push(path.join(file))
        }
    }

    // heightmap + tintmap are big files and should only be included if a full backup is requested
    if full_backup {
        if let DataMapping::File(file) = settings.heightmap() {
            filelist.push(path.join(file))
        }
        if let DataMapping::File(file) = settings.tintmap() {
            filelist.push(path.join(file))
        }
    }

    // find all foliage yml files
    if path.join("foliage").is_dir() {
        let mut foliage_filelist = WalkDir::new(path.join("foliage"))
            .contents_first(true)
            .into_iter()
            .filter_entry(|e| e.path().is_dir() || is_definition_file(e))
            .collect::<Result<Vec<_>, _>>()
            .map_err(|e| format!("backup creation: read error: {}", e))?;
        let mut foliage_filelist = foliage_filelist
            .drain(..)
            .map(|e| e.path().to_path_buf())
            .collect();

        filelist.append(&mut foliage_filelist);
    }

    if !filelist.is_empty() {
        let (filename, zipfile) = create_backupfile(path)?;

        // zip files as backup-Y-m-d_HMS.zip
        zip_files(path, &filelist, zipfile)
            .map_err(|e| format!("backup creation: error creating backup zip: {}", e))?;

        info!("definition backup stored as {}", filename.display());
    } else {
        info!("found no definition files for backup.");
    }
    Ok(())
}
// ----------------------------------------------------------------------------
use std::fs::File;
use std::io::{Seek, Write};
use std::path::{Path, PathBuf};

use bevy::prelude::*;

use walkdir::{DirEntry, WalkDir};

use super::{ProjectSettings, DataMapping};
// ----------------------------------------------------------------------------
fn create_backupfile(path: &Path) -> Result<(PathBuf, File), String> {
    use chrono::prelude::Local;
    use std::fs;

    if !path.is_dir() {
        return Err(format!(
            "backup creation: definition files path is not a directory: {}",
            path.display()
        ));
    }

    let mut backupfile = path.to_owned();
    backupfile.push("backups");

    fs::create_dir_all(backupfile.as_path()).map_err(|why| {
        format!(
            "could not create backup directory {}: {}",
            backupfile.display(),
            why
        )
    })?;

    backupfile.push(format!(
        "backup-{}.zip",
        Local::now().format("%Y-%m-%d_%H%M%S")
    ));

    let file = File::create(&backupfile)
        .map_err(|e| format!("backup creation: could not create backup file: {}", e))?;

    Ok((backupfile, file))
}
// ----------------------------------------------------------------------------
fn is_definition_file(entry: &DirEntry) -> bool {
    entry
        .file_name()
        .to_str()
        .map(|s| s.ends_with(".yml"))
        .unwrap_or(false)
}
// ----------------------------------------------------------------------------
fn zip_files<'entries, W>(
    src_dir: &Path,
    filelist: &'entries [PathBuf],
    writer: W,
) -> Result<Vec<&'entries Path>, String>
where
    W: Write + Seek,
{
    use std::io::Read;
    use zip::write::FileOptions;

    let mut zipped_files = Vec::with_capacity(filelist.len());

    let mut zip = zip::ZipWriter::new(writer);
    let options = FileOptions::default().compression_method(zip::CompressionMethod::Deflated);

    let mut buffer = Vec::new();
    for entry in filelist {
        let path = entry.as_path();

        if path.is_file() {
            let name = path
                .strip_prefix(src_dir)
                .map_err(|e| e.to_string())?
                .to_str()
                .ok_or_else(|| String::from("could not strip src dir from filename"))?;

            trace!(">> adding {}...", path.display());
            zip.start_file(name, options).map_err(|e| e.to_string())?;
            let mut f = File::open(path).map_err(|e| e.to_string())?;

            f.read_to_end(&mut buffer).map_err(|e| e.to_string())?;
            zip.write_all(&buffer).map_err(|e| e.to_string())?;

            buffer.clear();
            zipped_files.push(path);
        }
    }
    zip.finish()
        .map(|_| zipped_files)
        .map_err(|e| e.to_string())
}
// ----------------------------------------------------------------------------
