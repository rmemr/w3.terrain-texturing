// ----------------------------------------------------------------------------
use bevy::prelude::*;
use bevy::utils::HashMap;

pub use crate::terrain_material::{MaterialSlot, TerrainMaterialParam};
// ----------------------------------------------------------------------------
#[derive(Default)]
pub struct MaterialSetRegistry {
    pub(super) sets: HashMap<String, MaterialSetConfig>,
}
// ----------------------------------------------------------------------------
#[derive(Clone)]
pub struct MaterialSetConfig {
    /// path to base materialset (w2mg)
    id: String,
    /// (full) path to diffuse textures (size and type verified)
    diffuse: Vec<String>,
    /// (full) path to normal textures (size and type verified)
    normal: Vec<String>,
    /// materialsettings
    parameter: Vec<TerrainMaterialParam>,
}
// ----------------------------------------------------------------------------
#[allow(dead_code)]
impl MaterialSetConfig {
    // ------------------------------------------------------------------------
    pub fn new(
        id: String,
        diffuse: Vec<String>,
        normal: Vec<String>,
        parameter: Vec<TerrainMaterialParam>,
    ) -> Self {
        Self {
            id,
            diffuse,
            normal,
            parameter,
        }
    }
    // ------------------------------------------------------------------------
    #[inline(always)]
    pub fn id(&self) -> &str {
        &self.id
    }
    // ------------------------------------------------------------------------
    pub fn texture_size(&self) -> u32 {
        // TODO support other?
        1024
    }
    // ------------------------------------------------------------------------
    pub fn textures(&self) -> impl Iterator<Item = (MaterialSlot, &str, &str)> {
        self.diffuse
            .iter()
            .zip(self.normal.iter())
            .enumerate()
            .map(|(i, (diffuse, normal))| {
                (
                    MaterialSlot::from(i as u8),
                    diffuse.as_str(),
                    normal.as_str(),
                )
            })
    }
    // ------------------------------------------------------------------------
    pub fn parameters(&self) -> impl Iterator<Item = (MaterialSlot, &TerrainMaterialParam)> {
        self.parameter
            .iter()
            .enumerate()
            .map(|(i, p)| (MaterialSlot::from(i as u8), p))
    }
    // ------------------------------------------------------------------------
    pub fn set_parameter(&mut self, slot: MaterialSlot, new_param: TerrainMaterialParam) {
        if let Some(param) = self.parameter.get_mut(*slot as usize) {
            *param = new_param;
            // TODO set dirty flag if params differ from default?
        } else {
            warn!(
                "ignoring setting terrain material param for material slot {} without texture!",
                slot
            );
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl MaterialSetRegistry {
    // ------------------------------------------------------------------------
    pub fn get(&self, path: &str) -> Result<MaterialSetConfig, String> {
        self.sets.get(path).cloned().ok_or_else(|| {
            format!(
                "error loading material set settings: path [{}] not found.",
                path
            )
        })
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// systen
// ----------------------------------------------------------------------------
pub(super) fn setup_materialset_registry(mut registry: ResMut<MaterialSetRegistry>) {
    // TODO scan for available materialsets?
    registry
        .sets
        .insert("template".into(), MaterialSetConfig::default());
}
// ----------------------------------------------------------------------------
// defaults
// ----------------------------------------------------------------------------
impl Default for MaterialSetConfig {
    // ------------------------------------------------------------------------
    fn default() -> Self {
        // TODO provide two default materials in assets?
        Self {
            id: "default-materialset".to_string(),
            diffuse: vec![String::default(); 31],
            normal: vec![String::default(); 31],
            parameter: vec![TerrainMaterialParam::default(); 31],
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl MaterialSetConfig {
    // ------------------------------------------------------------------------
    pub(super) fn test_terrain() -> Self {
        let max_textures = 30;
        let base = "_test-data_/w3.textures/levels/prolog_village/prolog_village";
        let diffuse = (0..=max_textures.min(30))
            .map(|i| format!("{}.texarray.texture_{}.png", base, i))
            .collect::<Vec<_>>();

        let normal = (0..=max_textures.min(30))
            .map(|i| format!("{}_normals.texarray.texture_{}.png", base, i))
            .collect::<Vec<_>>();

        Self {
            id: "test-terrain".to_string(),
            diffuse,
            normal,
            parameter: test_terrain_material_params().to_vec(),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
#[allow(clippy::excessive_precision)]
fn test_terrain_material_params() -> [TerrainMaterialParam; 31] {
    [
        // 1
        TerrainMaterialParam {
            blend_sharpness: 0.3650000095,
            specularity_scale: 0.3225809932,
            specularity: 0.224999994,
            specularity_base: 0.5161290169,
            ..Default::default()
        },
        // 2
        TerrainMaterialParam {
            blend_sharpness: 0.163635999,
            specularity: 0.7170000076,
            specularity_base: 0.5279999971,
            ..Default::default()
        },
        // 3
        TerrainMaterialParam {
            blend_sharpness: 0.2060610056,
            slope_normal_dampening: 0.0121210003,
            specularity: 0.2460000068,
            specularity_base: 0.5360000134,
            ..Default::default()
        },
        // 4
        TerrainMaterialParam {
            blend_sharpness: 0.3220340014,
            slope_base_dampening: 0.2711859941,
            slope_normal_dampening: 0.4848479927,
            specularity: 0.2630000114,
            specularity_base: 0.5429999828,
            ..Default::default()
        },
        // 5
        TerrainMaterialParam {
            blend_sharpness: 0.1557790041,
            specularity: 0.0542169996,
            specularity_base: 0.566264987,
            ..Default::default()
        },
        // --- 6
        TerrainMaterialParam {
            blend_sharpness: 0.1700000018,
            specularity_scale: 0.0160000008,
            specularity: 0.0903609991,
            specularity_base: 0.566264987,
            ..Default::default()
        },
        // 7
        TerrainMaterialParam {
            specularity: 0.4169999957,
            specularity_base: 0.5099999905,
            ..Default::default()
        },
        // 8
        TerrainMaterialParam {
            blend_sharpness: 0.5921049714,
            slope_base_dampening: 0.5789470077,
            specularity_scale: 0.0160000008,
            specularity: 0.52700001,
            specularity_base: 0.5120000243,
            ..Default::default()
        },
        // 9
        TerrainMaterialParam {
            specularity: 0.3870970011,
            specularity_base: 0.5322579741,
            ..Default::default()
        },
        // 10
        TerrainMaterialParam {
            blend_sharpness: 0.1368419975,
            specularity_scale: 0.1700000018,
            specularity: 0.224999994,
            specularity_base: 0.4779999852,
            ..Default::default()
        },
        // --- 11
        TerrainMaterialParam {
            blend_sharpness: 0.1684210002,
            slope_base_dampening: 0.1894740015,
            specularity_scale: 0.903226018,
            specularity: 0.3870970011,
            specularity_base: 0.5645160079,
            ..Default::default()
        },
        // 12
        TerrainMaterialParam {
            blend_sharpness: 0.5368419886,
            slope_base_dampening: 0.400000006,
            specularity_scale: 0.8548390269,
            specularity: 0.370968014,
            specularity_base: 0.596773982,
            ..Default::default()
        },
        // 13
        TerrainMaterialParam {
            blend_sharpness: 0.1789470017,
            specularity: 0.4609999955,
            specularity_base: 0.5210000277,
            ..Default::default()
        },
        // 14
        TerrainMaterialParam {
            blend_sharpness: 0.3644070029,
            slope_base_dampening: 0.3644070029,
            specularity: 0.4838710129,
            specularity_base: 0.548386991,
            ..Default::default()
        },
        // 15
        TerrainMaterialParam {
            blend_sharpness: 0.2150000036,
            specularity_scale: 0.0869999975,
            specularity: 0.351000011,
            specularity_base: 0.4889999926,
            ..Default::default()
        },
        // --- 16
        TerrainMaterialParam {
            slope_normal_dampening: 0.2181819975,
            specularity_scale: 0.1640000045,
            specularity: 0.4230000079,
            specularity_base: 0.3619999886,
            ..Default::default()
        },
        // 17
        TerrainMaterialParam {
            blend_sharpness: 0.1199999973,
            specularity: 0.3790000081,
            specularity_base: 0.5490000248,
            ..Default::default()
        },
        // 18
        TerrainMaterialParam {
            blend_sharpness: 0.1757580042,
            slope_base_dampening: 0.9878789783,
            specularity: 0.5161290169,
            specularity_base: 0.5645160079,
            ..Default::default()
        },
        // 19
        TerrainMaterialParam {
            blend_sharpness: 0.1299999952,
            specularity_scale: 0.0049999999,
            specularity: 0.4720000029,
            specularity_base: 0.5870000124,
            ..Default::default()
        },
        // 20
        TerrainMaterialParam {
            blend_sharpness: 0.1052630022,
            slope_base_dampening: 0.1016950011,
            specularity: 0.1199999973,
            specularity_base: 0.5870000124,
            ..Default::default()
        },
        // --- 21
        TerrainMaterialParam {
            specularity_scale: 0.7741940022,
            specularity: 0.1612900048,
            specularity_base: 0.419355005,
            ..Default::default()
        },
        // 22
        TerrainMaterialParam {
            specularity: 0.3449999988,
            specularity_base: 0.5640000105,
            ..Default::default()
        },
        // 23
        TerrainMaterialParam {
            specularity: 0.4169999957,
            specularity_base: 0.5490000248,
            ..Default::default()
        },
        // 24
        TerrainMaterialParam {
            ..Default::default()
        },
        // 25
        TerrainMaterialParam {
            blend_sharpness: 0.4322029948,
            slope_base_dampening: 0.4067800045,
            specularity: 0.3619999886,
            specularity_base: 0.5149999857,
            ..Default::default()
        },
        // --- 26
        TerrainMaterialParam {
            blend_sharpness: 0.174999997,
            specularity: 0.370968014,
            specularity_base: 0.5645160079,
            falloff: 0.0549999997,
            ..Default::default()
        },
        // 27
        TerrainMaterialParam {
            blend_sharpness: 0.3449999988,
            specularity_scale: 0.2586210072,
            specularity: 0.419355005,
            specularity_base: 0.4838710129,
            falloff: 0.3620690107,
            ..Default::default()
        },
        // 28
        TerrainMaterialParam {
            blend_sharpness: 0.1319440007,
            specularity: 0.3680000007,
            specularity_base: 0.5049999952,
            ..Default::default()
        },
        // 29
        TerrainMaterialParam {
            specularity_base: 0.5049999952,
            ..Default::default()
        },
        // 30
        TerrainMaterialParam {
            specularity_scale: 0.4720000029,
            specularity: 0.4449999928,
            specularity_base: 0.4779999852,
            ..Default::default()
        },
        // --- 31
        TerrainMaterialParam {
            specularity: 0.4889999926,
            specularity_base: 0.5490000248,
            ..Default::default()
        },
    ]
}
// ----------------------------------------------------------------------------
