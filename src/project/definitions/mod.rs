// ----------------------------------------------------------------------------
use std::path::Path;

use bevy::prelude::*;
use bevy::utils::HashMap;

use yaml_utils as yaml;

use crate::environment::EnvironmentConfigRegistry;
use crate::terrain_material::MaterialSlot;
use crate::terrain_render::TerrainMaterialParam;

use super::{shorten, MaterialSetConfig, MaterialSetRegistry, ProjectSettings, Result};
// ----------------------------------------------------------------------------
mod reader;
mod writer;

mod world;

#[allow(dead_code, unused_macros)]
mod validator;
// ----------------------------------------------------------------------------
pub fn load_def(
    fullpath: &Path,
    envdef_registry: &EnvironmentConfigRegistry,
    materialset_registry: &MaterialSetRegistry,
) -> Result<ProjectSettings> {
    info!("loading {}", fullpath.display());

    let err_msg = |e| -> String {
        format!(
            "Failed to read definition:\n\n{}\n\n{}",
            shorten::<64>(fullpath.display().to_string()),
            e
        )
    };

    ProjectSettings::read(fullpath, envdef_registry, materialset_registry).map_err(err_msg)
}
// ----------------------------------------------------------------------------
pub fn save_def(directory: &Path, project: &ProjectSettings) -> Result<()> {
    info!("saving project settings in {}... ", directory.display());

    let err_msg = |e| -> String {
        format!(
            "Failed to store definition:\n\n{}\n\n{}",
            shorten::<64>(
                directory
                    .join(project.world_definition_file())
                    .display()
                    .to_string()
            ),
            e
        )
    };

    ProjectSettings::store(directory, project).map_err(err_msg)
}
// ----------------------------------------------------------------------------
/// Trait for deserializing a data type struct from yaml
trait TryFromYaml<D, R = Self>
where
    R: Sized,
{
    fn try_from_yaml(data: &D) -> Result<R>;
}
// ----------------------------------------------------------------------------
trait IntoYaml {
    type Output;

    #[allow(clippy::wrong_self_convention)]
    fn into_yaml(&self) -> Self::Output;
}
// ----------------------------------------------------------------------------
// unsupported definition data parts are treated as rawdata (even if it is valid yml)
// for now. these helper functions convert between a "raw" string and the yml defintion
// ----------------------------------------------------------------------------
#[derive(Default, Debug, Clone)]
pub(super) struct UnusedDefinitionData(HashMap<String, yaml::Yaml>);
// ----------------------------------------------------------------------------
impl UnusedDefinitionData {
    // ------------------------------------------------------------------------
    fn add(&mut self, key: &str, data: &yaml::Yaml) {
        self.0.insert(key.into(), data.to_owned());
    }
    // ------------------------------------------------------------------------
    fn retrieve(&mut self, key: &str) -> yaml::Yaml {
        self.0.remove(key).unwrap_or(yaml::Yaml::Null)
    }
    // ------------------------------------------------------------------------
    fn retrieve_or_default(&mut self, key: &str, default: yaml::Yaml) -> yaml::Yaml {
        self.0.remove(key).unwrap_or(default)
    }
    // ------------------------------------------------------------------------
    fn drain(&mut self) -> impl Iterator<Item = (String, yaml::Yaml)> + '_ {
        self.0.drain()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
#[allow(dead_code)]
fn rawdata_to_string(data: &yaml::Yaml) -> Result<String> {
    use yaml::encoder::YamlEmitter;

    let mut data_str = String::new();

    let mut emitter = YamlEmitter::new(&mut data_str);
    emitter
        .dump(data)
        .map_err(|e| format!("rawdata deserializer: {:?}", e))?;

    Ok(data_str)
}
// ----------------------------------------------------------------------------
#[allow(dead_code)]
fn rawdata_to_yaml(data: &str) -> Result<Option<yaml::Yaml>> {
    yaml::parser::YamlLoader::load_from_str(data)
        .map(|mut list| list.pop())
        .map_err(|e| format!("rawdata deserializer: {:?}", e))
}
// ----------------------------------------------------------------------------
