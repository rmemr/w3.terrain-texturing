//
// definitions::reader
//
// ----------------------------------------------------------------------------
use std::path::Path;

use super::{EnvironmentConfigRegistry, MaterialSetConfig, MaterialSetRegistry, ProjectSettings};
use super::{Result, TryFromYaml};

use super::world::WorldDefinition;
use super::yaml;
// ----------------------------------------------------------------------------
impl yaml::YamlLoader for ProjectSettings {}
// ----------------------------------------------------------------------------
impl ProjectSettings {
    // ------------------------------------------------------------------------
    pub(super) fn read(
        fullpath: &Path,
        envdef_registry: &EnvironmentConfigRegistry,
        materialset_registry: &MaterialSetRegistry,
    ) -> Result<Self> {
        use yaml::YamlLoader;

        let data = Self::read_yaml(&fullpath.to_string_lossy())?;

        // parse and load project definiton
        let world_definition = WorldDefinition::try_from_yaml(&data)?;

        // convert into useable project settings
        Self::create_project_settings(
            fullpath,
            world_definition,
            envdef_registry,
            materialset_registry,
        )
    }
    // ------------------------------------------------------------------------
    fn create_project_settings(
        world_definition_path: &Path,
        definition: WorldDefinition,
        envdef_registry: &EnvironmentConfigRegistry,
        materialset_registry: &MaterialSetRegistry,
    ) -> Result<ProjectSettings> {
        let clipmap_levels = ((definition.terrain.map_size as f32).log2() - 9.0) as u8;

        let environment = if envdef_registry.contains(&definition.env_def) {
            Some(definition.env_def)
        } else {
            None
        };

        let mut materialset =
            if let Ok(material_set) = materialset_registry.get(&definition.terrain.materialset) {
                material_set
            } else {
                MaterialSetConfig::default()
            };

        // update materialset textureparams
        for (slot, params) in definition.terrain.texture_params {
            materialset.set_parameter(slot, params);
        }

        let mut settings = ProjectSettings::try_new(
            &definition.name,
            world_definition_path.to_path_buf(),
            definition.terrain.map_size,
            definition.terrain.terrain_size,
            definition.terrain.min_height,
            definition.terrain.max_height,
            definition.terrain.heightmap,
            definition.terrain.tintmap,
            definition.terrain.textures,
            materialset,
            environment,
            clipmap_levels,
        )?;
        settings
            .unused_definitions
            .insert("world-def", definition.unused);
        settings
            .unused_definitions
            .insert("terrain-def", definition.terrain.unused);

        Ok(settings)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
