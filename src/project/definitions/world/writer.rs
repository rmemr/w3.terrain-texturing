// ----------------------------------------------------------------------------
use bevy::prelude::warn;
use bevy::utils::HashMap;

use super::yaml::encoder::{IntoYaml as ExtIntoYaml, MapEncoder};
use super::yaml::Yaml;

use super::{
    DataMapping, IntoYaml, MaterialSlot, TerrainDefinition, TerrainMaterialParam, TextureMaps,
    WorldDefinition,
};
// ----------------------------------------------------------------------------
impl IntoYaml for WorldDefinition {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        let mut def = MapEncoder::new();
        let mut unused = self.unused.clone();

        def.add("name", self.name.as_str());
        def.add("worldpath", unused.retrieve("worldpath"));
        def.add("terrain", self.terrain.into_yaml());

        for (key, value) in unused.drain() {
            let value = value.clone();

            // patch environment parameters with env def
            if key.to_lowercase() == "environmentparameters" {
                let mut env_parms = MapEncoder::new();
                if let Some(mut value) = value.into_hash() {
                    for (key, value) in value.drain() {
                        if let Some(key) = key.as_str() {
                            if key.to_lowercase() == "environmentDefinition" {
                                env_parms.add(key, self.env_def.as_str());
                            } else {
                                env_parms.add(key, value);
                            }
                        } else {
                            warn!("failed to convert key to str: {:?}", key);
                        }
                    }
                } else {
                    warn!("failed to extract environmentParameters from world definition settings");
                    env_parms.add("environmentDefinition", self.env_def.as_str());
                }

                def.add("environmentParameters", env_parms);
            } else {
                def.add(key, value);
            }
        }

        MapEncoder::from_key_value("WorldDefinition", def).intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for TerrainDefinition {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    #[rustfmt::skip]
    fn into_yaml(&self) -> Self::Output {
        let mut def = MapEncoder::new();
        let mut unused = self.unused.clone();

        def.add("heightfield", self.heightmap.into_yaml());
        def.add("colormap", self.tintmap.into_yaml());
        def.add("textures", self.textures.into_yaml());
        def.add("terrainSize", self.terrain_size);
        def.add("minHeight", self.min_height);
        def.add("maxHeight", self.max_height);

        // for example projects some information is not available -> provide defaults
        let tile_res = Yaml::Integer(1024);
        let tiles_per_edge = Yaml::Integer(self.map_size as i64 / 1024);
        let clip_size = Yaml::Integer(1024);
        let clip_levels = Yaml::Integer(((self.map_size as f32).log2() - 10.0) as i64);
        let color_start_mip = Yaml::Integer(0);

        def.add("tileRes", unused.retrieve_or_default("tileRes", tile_res));
        def.add("numTilesPerEdge", unused.retrieve_or_default("numTilesPerEdge", tiles_per_edge));
        def.add("clipSize", unused.retrieve_or_default("clipSize", clip_size));
        def.add("numClipmapStackLevels", unused.retrieve_or_default("numClipmapStackLevels", clip_levels));
        def.add("colormapStartingMip",unused.retrieve_or_default("colormapStartingMip", color_start_mip));

        def.add("material", self.materialset.as_str());
        def.add("textureParams", self.texture_params.into_yaml());

        // remaining unsued
        for (key, value) in unused.drain() {
            def.add(key, value);
        }

        def.intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<T: ExtIntoYaml + Copy> IntoYaml for DataMapping<T> {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        match self {
            DataMapping::None => Yaml::Null,
            DataMapping::File(f) => f.to_string_lossy().to_string().intoyaml(),
            DataMapping::Const(v) => (*v).intoyaml(),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for HashMap<MaterialSlot, TerrainMaterialParam> {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        let mut def = MapEncoder::new();

        // textureparams may not necessary be available for all textures slots in
        // the definitions
        for slot in 0..32 {
            let slot = MaterialSlot::from(slot);
            if let Some(params) = self.get(&slot) {
                def.add(format!("texture_{:0>2}", *slot + 1), params.into_yaml());
            }
        }

        def.intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for TextureMaps {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        let mut def = MapEncoder::new();

        def.add("background", self.background.into_yaml());
        def.add("overlay", self.overlay.into_yaml());
        def.add("blendcontrol", self.blendcontrol.into_yaml());

        def.intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for TerrainMaterialParam {
    type Output = Yaml;
    // ------------------------------------------------------------------------
    fn into_yaml(&self) -> Self::Output {
        let mut def = MapEncoder::new();

        def.add("blendSharpness", self.blend_sharpness);
        def.add("slopeBasedDamp", self.slope_base_dampening);
        def.add("slopeBasedNormalDamp", self.slope_normal_dampening);
        def.add("rspecBase", self.specularity_base);
        def.add("rspecScale", self.specularity_scale);
        def.add("specularity", self.specularity);
        def.add("falloff", self.falloff);

        def.intoyaml()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
