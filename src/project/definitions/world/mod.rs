//
// definitions::world
//
// ----------------------------------------------------------------------------
// yml model
// ----------------------------------------------------------------------------
#[derive(Default)]
pub(super) struct WorldDefinition {
    pub name: String,
    pub terrain: TerrainDefinition,
    pub env_def: String,
    // data embedded in yml but not necessarily required/used
    pub unused: UnusedDefinitionData,
}
// ----------------------------------------------------------------------------
#[derive(Default)]
pub(super) struct TerrainDefinition {
    pub heightmap: DataMapping<u16>,
    pub tintmap: DataMapping<u32>,
    pub textures: TextureMaps,
    pub map_size: u32,
    pub terrain_size: f32,
    pub min_height: f32,
    pub max_height: f32,
    pub materialset: String,
    pub texture_params: HashMap<MaterialSlot, TerrainMaterialParam>,
    // data embedded in yml but not necessarily required/used
    pub unused: UnusedDefinitionData,
}
// ----------------------------------------------------------------------------
mod reader;
mod writer;
// ----------------------------------------------------------------------------
impl WorldDefinition {
    // ------------------------------------------------------------------------
    fn set_env(&mut self, env: &str) {
        self.env_def = normalize_path(env);
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
use bevy::utils::HashMap;

use crate::{loader::DataMapping, project::TextureMaps};

use super::yaml;
use super::{IntoYaml, Result, TryFromYaml, UnusedDefinitionData};
use super::{MaterialSlot, TerrainMaterialParam};
// ----------------------------------------------------------------------------
fn normalize_path(path: &str) -> String {
    path.replace('\\', "/")
        .replace("//", "/")
        .trim_start_matches('/')
        .trim_end_matches('/')
        .to_lowercase()
}
// ----------------------------------------------------------------------------
// validation
// ----------------------------------------------------------------------------
use super::validator::*;
// ----------------------------------------------------------------------------
impl ValidatableElement for WorldDefinition {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        validate_str(
            &self.name,
            &format!("{}/name", id),
            &[
                is_nonempty(),
                chars("^[a-z_]*$", "[a-z_]"),
                min_length(3),
                max_length(25),
            ],
        )?;

        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for TerrainDefinition {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        // check data mappings
        if let DataMapping::None = self.heightmap {
            validate_err_missing!(id, "heightfield");
        }
        if let DataMapping::None = self.tintmap {
            validate_err_missing!(id, "colormap");
        }
        self.textures.validate(&format!("{}/textures", id))?;

        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for TextureMaps {
    // ------------------------------------------------------------------------
    fn validate(&self, _: &str) -> Result<()> {
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for TerrainMaterialParam {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        for (v, caption) in [
            (self.blend_sharpness, "blendSharpness"),
            (self.slope_base_dampening, "slopeBasedDamp"),
            (self.slope_normal_dampening, "slopeBasedNormalDamp"),
            (self.specularity_scale, "rspecScale"),
            (self.specularity, "specularity"),
            (self.specularity_base, "rspecBase"),
            (self.falloff, "falloff"),
        ] {
            let path = format!("{}/{}", id, caption);
            validate_float(v, &path, &[float_is_between(0.0, 1.0)])?;
        }

        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
