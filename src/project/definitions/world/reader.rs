//
// definitions::world::reader
//
// ----------------------------------------------------------------------------
use std::convert::TryFrom;
use std::path::PathBuf;

use bevy::utils::HashMap;
use yaml_utils::parser::{MapParser, Parser, YamlType};
use yaml_utils::yaml_errmsg;
use yaml_utils::Yaml;

use crate::{loader::DataMapping, project::TextureMaps};

use super::{MaterialSlot, TerrainDefinition, TerrainMaterialParam, WorldDefinition};
use super::{Result, TryFromYaml, ValidatableElement};

use crate::project::definitions::yaml::*;
// ----------------------------------------------------------------------------
// TryFromYaml impl
// ----------------------------------------------------------------------------
impl TryFromYaml<Yaml> for WorldDefinition {
    // ------------------------------------------------------------------------
    fn try_from_yaml(data: &Yaml) -> Result<Self> {
        let mut result = Self::default();

        let data = MapParser::new("", data)?;

        for (key, value) in data.iter().flat_map(|d| d.to_key_value()) {
            match key.to_lowercase().as_str() {
                "worlddefinition" => {
                    let data = value.as_map()?;
                    for (key, value) in data.iter().flat_map(|d| d.to_key_value()) {
                        match key.to_lowercase().as_str() {
                            ".type" => {} // ignore
                            "name" => result.name = value.to_lowercase_string()?,
                            "terrain" => {
                                result.terrain = TerrainDefinition::try_from_yaml(&value)?;
                            }
                            "environmentparameters" => {
                                // extract env definition
                                if let Some(Ok(def)) =
                                    value.as_map()?.get_value("environmentdefinition")
                                {
                                    result.set_env(def.as_str()?);
                                }
                                result.unused.add(key, value.data());
                            }
                            _ => {
                                result.unused.add(key, value.data());
                            }
                        }
                    }
                }
                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }
        result.validate("world definition").and(Ok(result))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'data> TryFromYaml<Parser<'data>> for TerrainDefinition {
    // ------------------------------------------------------------------------
    fn try_from_yaml(data: &Parser) -> Result<Self> {
        let mut result = TerrainDefinition::default();

        // final map size will be calculated from this
        let mut tile_res = 0;
        let mut tiles = 0;

        let data = data.as_map()?;
        for (key, value) in data.iter().flat_map(|d| d.to_key_value()) {
            match key.to_lowercase().as_str() {
                "textures" => result.textures = TextureMaps::try_from_yaml(&value)?,
                "colormap" => result.tintmap = DataMapping::try_from_yaml(&value)?,
                "heightfield" => result.heightmap = DataMapping::try_from_yaml(&value)?,
                "terrainsize" => result.terrain_size = value.to_float()?,
                "minheight" => result.min_height = value.to_float()?,
                "maxheight" => result.max_height = value.to_float()?,
                "material" => {
                    result.materialset = super::normalize_path(value.as_str()?);
                }
                "textureparams" => {
                    result.texture_params = HashMap::try_from_yaml(&value)?;
                }
                "tileres" => {
                    tile_res = value.to_integer()?;
                    result.unused.add(key, value.data());
                }
                "numtilesperedge" => {
                    tiles = value.to_integer()?;
                    result.unused.add(key, value.data());
                }
                _ => {
                    result.unused.add(key, value.data());
                }
            }
        }
        // calculate map size
        if tiles < 1 {
            return yaml_err_not_found!(data.id(), "numTilesPerEdge");
        }
        if tile_res < 256 {
            return yaml_err_not_found!(data.id(), "tileRes");
        }
        result.map_size = (tiles * tile_res) as u32;

        result.validate("terrain").and(Ok(result))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for HashMap<MaterialSlot, TerrainMaterialParam> {
    // ------------------------------------------------------------------------
    fn try_from_yaml(data: &Parser) -> Result<Self> {
        let mut result = HashMap::new();

        let data = data.as_map()?;
        for (key, value) in data.iter().flat_map(|d| d.to_key_value()) {
            let slot = MaterialSlot::try_from(key).map_err(|e| yaml_errmsg!(value.id(), e))?;
            let params = TerrainMaterialParam::try_from_yaml(&value)?;

            result.insert(slot, params);
        }

        Ok(result)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFrom<&'a str> for MaterialSlot {
    type Error = String;
    // ------------------------------------------------------------------------
    fn try_from(value: &'a str) -> Result<Self> {
        match value.to_lowercase().strip_prefix("texture_") {
            Some(v) => v
                .trim()
                .parse::<u8>()
                .map_err(|e| format!("failed to parse texture slot [{}]: {}", value, e))
                .map(|i| MaterialSlot::from(i - 1)),
            None => Err(format!("failed to parse texture slot [{}]", value)),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for TerrainMaterialParam {
    // ------------------------------------------------------------------------
    fn try_from_yaml(data: &Parser) -> Result<Self> {
        let mut result = Self::default();

        let data = data.as_map()?;
        for (key, value) in data.iter().flat_map(|d| d.to_key_value()) {
            match key.to_lowercase().as_str() {
                "blendsharpness" => result.blend_sharpness = value.to_float()?,
                "slopebaseddamp" => result.slope_base_dampening = value.to_float()?,
                "slopebasednormaldamp" => result.slope_normal_dampening = value.to_float()?,
                "rspecscale" => result.specularity_scale = value.to_float()?,
                "specularity" => result.specularity = value.to_float()?,
                "rspecbase" => result.specularity_base = value.to_float()?,
                "falloff" => result.falloff = value.to_float()?,
                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }
        result.validate(data.id()).and(Ok(result))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> TryFromYaml<Parser<'a>> for TextureMaps {
    // ------------------------------------------------------------------------
    fn try_from_yaml(data: &Parser) -> Result<Self> {
        let mut texturing = TextureMaps::default();

        for kvparser in data.as_map()?.iter() {
            let (key, value) = kvparser.to_key_value()?;

            match key.to_lowercase().as_str() {
                "background" => texturing.background = DataMapping::try_from_yaml(&value)?,
                "overlay" => texturing.overlay = DataMapping::try_from_yaml(&value)?,
                "blendcontrol" => texturing.blendcontrol = DataMapping::try_from_yaml(&value)?,
                _ => return yaml_err_unknown_key!(data.id(), key),
            }
        }
        texturing.validate(data.id()).and(Ok(texturing))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'data, T: TryFrom<i64> + std::fmt::Debug> TryFromYaml<Parser<'data>> for DataMapping<T> {
    // ------------------------------------------------------------------------
    fn try_from_yaml(data: &Parser) -> Result<Self> {
        match data.datatype() {
            YamlType::Integer => Ok(DataMapping::Const(T::try_from(data.to_long()?).map_err(
                |_| yaml_errmsg!(data.id(), "failed to convert to expected datarange."),
            )?)),
            _ => Ok(DataMapping::File(PathBuf::from(data.to_string()?))),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
