//
// definitions::writer
//
// ----------------------------------------------------------------------------
use std::path::Path;

use bevy::utils::HashMap;

use super::{IntoYaml, Result};
use super::{ProjectSettings, UnusedDefinitionData};

use super::world::{TerrainDefinition, WorldDefinition};
use super::yaml::YamlWriter;
// ----------------------------------------------------------------------------
impl YamlWriter for ProjectSettings {}
// ----------------------------------------------------------------------------
impl ProjectSettings {
    // ------------------------------------------------------------------------
    pub(super) fn store(outputdir: &Path, settings: &ProjectSettings) -> Result<()> {
        use chrono::prelude::Local;

        let creation_time = Local::now().format("%Y-%m-%d %H:%M:%S").to_string();

        if outputdir.is_dir() {
            Self::write_yaml(
                &outputdir.join(settings.world_definition_file()),
                &format!(
                    "#\n# world definition for: {}\n# \n# (saved: {})\n#\n",
                    settings.name(),
                    creation_time
                ),
                &settings.create_world_definition().into_yaml(),
            )?;
            Ok(())
        } else {
            Err(format!(
                "definition writer: [{}] is not a valid directory",
                outputdir.display()
            ))
        }
    }
    // ------------------------------------------------------------------------
    fn create_world_definition(&self) -> WorldDefinition {
        let mut def = WorldDefinition {
            name: self.name.to_owned(),
            // worldpath: self
            terrain: TerrainDefinition {
                heightmap: self.heightmap.clone(),
                tintmap: self.tintmap.clone(),
                textures: self.texturemaps.clone(),
                map_size: self.terrain.map_size(),
                terrain_size: self.terrain.terrain_size(),
                min_height: self.terrain.min_height(),
                max_height: self.terrain.max_height(),
                materialset: self.materialset.id().to_owned(),
                texture_params: self
                    .materialset
                    .parameters()
                    .map(|(slot, params)| (slot, *params))
                    .collect::<HashMap<_, _>>(),
                unused: UnusedDefinitionData(HashMap::default()),
            },
            env_def: self.environment.as_ref().cloned().unwrap_or_default(),
            unused: UnusedDefinitionData(HashMap::default()),
        };

        // restore unsused settings in stored definitions
        def.unused = self
            .unused_definitions
            .get("world-def")
            .cloned()
            .unwrap_or_default();

        def.terrain.unused = self
            .unused_definitions
            .get("terrain-def")
            .cloned()
            .unwrap_or_default();

        def
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
