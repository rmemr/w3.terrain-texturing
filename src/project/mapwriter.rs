// ----------------------------------------------------------------------------
use std::fs;
use std::fs::File;
use std::path::{Path, PathBuf};

use bevy::prelude::*;
use bevy::tasks::AsyncComputeTaskPool;

use bytemuck::cast_slice;
use byteorder::{BigEndian, ByteOrder, NativeEndian};
use png::{BitDepth, ColorType, Compression};

use super::{MapType, Result, StoreMapTask, TextureMaps};
// ----------------------------------------------------------------------------
pub(super) fn store_heightmap(
    thread_pool: &AsyncComputeTaskPool,
    destination_path: &Path,
    filename: &Path,
    mapsize: u32,
    data: &[u16],
) -> StoreMapTask {
    let fullpath = destination_path.join(filename);

    // png crate requires u8 slice in big endian order as input data
    // reorder to big endian (src: image crate)
    // https://docs.rs/image/latest/src/image/codecs/png.rs.html#620-654
    let mut reordered = vec![0; data.len() * 2];
    cast_slice(data)
        .chunks(2)
        .zip(reordered.chunks_mut(2))
        .for_each(|(b, r)| BigEndian::write_u16(r, NativeEndian::read_u16(b)));

    let task = thread_pool.spawn(async move {
        store_image(
            &fullpath,
            &reordered,
            mapsize,
            mapsize,
            ColorType::Grayscale,
            BitDepth::Sixteen,
            Compression::Default,
            None,
        )
        .map(|_| fullpath)
    });

    StoreMapTask(MapType::Heightmap, task)
}
// ----------------------------------------------------------------------------
pub(super) fn store_tintmap(
    thread_pool: &AsyncComputeTaskPool,
    destination_path: &Path,
    filename: &Path,
    mapsize: u32,
    data: &[u8],
) -> StoreMapTask {
    let fullpath = destination_path.join(filename);
    let data = data.to_vec();
    let task = thread_pool.spawn(async move {
        store_image(
            &fullpath,
            &data,
            mapsize,
            mapsize,
            ColorType::Rgba,
            BitDepth::Eight,
            Compression::Default,
            None,
        )
        .map(|_| fullpath)
    });

    StoreMapTask(MapType::Tintmap, task)
}
// ----------------------------------------------------------------------------
pub(super) fn store_texturemaps(
    thread_pool: &AsyncComputeTaskPool,
    destination_path: &Path,
    mapsize: u32,
    texturemaps: &TextureMaps,
    data: &[u16],
) -> Result<[StoreMapTask; 3]> {
    assert_eq!(data.len(), (mapsize * mapsize) as usize);

    let size = mapsize as usize;
    let overlay_mapfile = texturemaps.overlay().as_path()?;
    let background_mapfile = texturemaps.background().as_path()?;
    let blendcontrol_mapfile = texturemaps.blendcontrol().as_path()?;

    let compression = png::Compression::Default;

    // split into dedicated textures
    // 0..4 overlay texture idx
    // 5..9 background textures idx
    // 10..15 blend control
    //   10..12 slope threshold
    //   13..15 UV scale

    let mut overlay = Vec::with_capacity(size * size);
    let mut background = Vec::with_capacity(size * size);
    let mut blendcontrol = Vec::with_capacity(size * size);

    for v in data {
        overlay.push((v & 0b1_1111) as u8);
        background.push((v >> 5 & 0b1_1111) as u8);
        blendcontrol.push((v >> 10 & 0b11_1111) as u8);
    }

    let overlay_file = destination_path.join(overlay_mapfile);
    let overlay_task = thread_pool.spawn(async move {
        store_image(
            &overlay_file,
            &overlay,
            mapsize,
            mapsize,
            ColorType::Indexed,
            BitDepth::Eight,
            compression,
            Some(&PALETTE_TEXTURING),
        )
        .map(|_| overlay_file)
    });

    let background_file = destination_path.join(background_mapfile);
    let background_task = thread_pool.spawn(async move {
        store_image(
            &background_file,
            &background,
            mapsize,
            mapsize,
            ColorType::Indexed,
            BitDepth::Eight,
            compression,
            Some(&PALETTE_TEXTURING),
        )
        .map(|_| background_file)
    });

    let blendcontrol_file = destination_path.join(blendcontrol_mapfile);
    let blendcontrol_task = thread_pool.spawn(async move {
        store_image(
            &blendcontrol_file,
            &blendcontrol,
            mapsize,
            mapsize,
            ColorType::Indexed,
            BitDepth::Eight,
            compression,
            Some(&generate_palette(64)),
        )
        .map(|_| blendcontrol_file)
    });

    Ok([
        StoreMapTask(MapType::Texturing, overlay_task),
        StoreMapTask(MapType::Texturing, background_task),
        StoreMapTask(MapType::Texturing, blendcontrol_task),
    ])
}
// ----------------------------------------------------------------------------
fn generate_palette(count: usize) -> Vec<u8> {
    let mut palette = Vec::with_capacity(count * 3);
    for i in 0..count {
        let scale = (i % 8) as u8;
        let slope = ((i / 8) % 8) as u8;
        palette.push(32 + 255 / 8 * scale);
        palette.push(55 + scale * slope * 4);
        palette.push(32 + 255 / 8 * slope);
        // alternative colors
        // palette_blendcontrol.push(255 / 8 * scale);
        // palette_blendcontrol.push(255 - 255 / 8 * slope);
        // palette_blendcontrol.push(255 - 255 / 8 * scale);
    }
    palette
}
// ----------------------------------------------------------------------------
#[allow(clippy::too_many_arguments)]
fn store_image(
    path: &Path,
    data: &[u8],
    width: u32,
    height: u32,
    colortype: ColorType,
    bitdepth: BitDepth,
    compression: Compression,
    palette: Option<&[u8]>,
) -> Result<()> {
    use std::io::BufWriter;

    info!("creating {}...", path.display());

    let mut outputdir = PathBuf::from(path);
    outputdir.pop();

    fs::create_dir_all(outputdir.as_path()).map_err(|why| {
        format!(
            "failed to create outputdir {}: {}",
            outputdir.display(),
            why
        )
    })?;

    let target = File::create(path).map_err(|e| format!("failed to create file: {}", e))?;

    let mut encoder = png::Encoder::new(BufWriter::new(target), width, height);
    encoder.set_color(colortype);
    if let ColorType::Indexed = colortype {
        encoder.set_palette(palette.expect("palette colors").to_vec());
    }
    encoder.set_depth(bitdepth);
    encoder.set_compression(compression);

    let err_msg = |e| -> String {
        format!(
            "{}: failed to create image: {}",
            path.file_name().unwrap().to_string_lossy(),
            e
        )
    };

    let mut writer = encoder.write_header().map_err(err_msg)?;
    writer.write_image_data(data).map_err(err_msg)
}
// ----------------------------------------------------------------------------
type TexturePalette = [u8; 32 * 3];

#[rustfmt::skip]
const PALETTE_TEXTURING: TexturePalette = [
    0, 0, 0,        62, 65, 30,     180, 174, 139,
    148, 157,       172, 156, 152,  145, 112, 134,
    76, 46, 66,     8, 110, 103,    90, 79, 69,
    54, 96, 93,     81, 89, 97,     59, 138, 125,
    106, 94, 91,    76, 152, 144,   86, 120, 120,
    113, 95, 88,    82, 162, 153,   137, 123, 113,
    95, 125, 120,   120, 185, 152,  104, 142, 127,
    106, 107, 98,   95, 106, 105,   100, 136, 126,
    113, 102, 102,  98, 138, 135,   130, 95, 89, 78,
    119, 112, 96,   117, 98, 69,    155, 132, 104,
    184, 160, 128,  127, 123, 98,
];
// ----------------------------------------------------------------------------
