// ----------------------------------------------------------------------------
pub(super) fn setup_saving(
    mut project: ResMut<ProjectSettings>,
    materialset: Res<TerrainMaterialSet>,
    mut editor_events: EventWriter<EditorEvent>,
    mut task_manager: ResMut<cmds::AsyncCommandManager>,
    mut progress: ResMut<AsyncTaskProgress>,
) -> Result<()> {
    // if project settings are loaded with placeholder values for maps these need
    // to be replaced to be now files
    project.setup_maps_as_files();

    // store updated definitions, e.g. textureparams in world def
    super::save_def(project.project_directory(), &project)?;

    // also update materialset params
    for (slot, param) in materialset.parameter.iter().enumerate() {
        project.update_material_parameter((slot as u8).into(), *param);
    }

    let progress_events = if project.modified.heightmap || project.modified.tintmap {
        // full save
        progress.reset().watch(vec![
            (MapType::Heightmap, 1),
            (MapType::Texturing, 3),
            (MapType::Tintmap, 1),
        ]);

        // queue tasks
        task_manager.add_new(cmds::WaitForProjectFullySaved.into());
        task_manager.add_new(cmds::SaveTintmap.into());
        task_manager.add_new(cmds::SaveTextureMaps.into());
        task_manager.add_new(cmds::SaveHeightmap.into());

        vec![
            cmds::TrackedProgress::SavedTintmap(false),
            cmds::TrackedProgress::SavedTextureMaps(0, 3),
            cmds::TrackedProgress::SavedHeightmap(false),
        ]
    } else {
        // quicksave
        progress.reset().watch(vec![(MapType::Texturing, 3)]);

        // queue tasks
        task_manager.add_new(cmds::WaitForProjectQuickSaved.into());
        task_manager.add_new(cmds::SaveTextureMaps.into());

        vec![cmds::TrackedProgress::SavedTextureMaps(0, 3)]
    };

    // bigger terrains may take > 2 mins of saving -> show a progress bar by
    // tracking all longer running events
    editor_events.send(EditorEvent::ProgressTrackingStart(
        "Saving Project".into(),
        progress_events,
    ));
    Ok(())
}
// ----------------------------------------------------------------------------
#[allow(clippy::too_many_arguments)]
pub(super) fn start_async_store_map_tasks(
    mut commands: Commands,
    project: Res<ProjectSettings>,
    texture_clipmap: Res<TextureControlClipmap>,
    tint_clipmap: Res<TintClipmap>,
    heightmap_clipmap: Res<HeightmapClipmap>,
    mut tasks_queued: EventReader<AsyncTaskStartEvent>,
) -> Result<()> {
    use AsyncTaskStartEvent::*;
    let thread_pool = AsyncComputeTaskPool::get();

    for task in tasks_queued.iter() {
        match task {
            SaveTextureMaps => {
                debug!("saving texturing maps...");

                let [overlay_task, background_task, blendcontrol_task] =
                    mapwriter::store_texturemaps(
                        thread_pool,
                        project.project_directory(),
                        project.terrain_config().map_size(),
                        project.texturemaps(),
                        texture_clipmap.fullres_data_slice(),
                    )?;

                commands.spawn().insert(overlay_task);
                commands.spawn().insert(background_task);
                commands.spawn().insert(blendcontrol_task);
            }
            SaveHeightmap => {
                debug!("saving heightmap...");

                let task = mapwriter::store_heightmap(
                    thread_pool,
                    project.project_directory(),
                    project.heightmap().as_path()?,
                    project.terrain_config().map_size(),
                    heightmap_clipmap.fullres_data_slice(),
                );
                commands.spawn().insert(task);
            }
            SaveTintmap => {
                debug!("saving tintmap...");

                let task = mapwriter::store_tintmap(
                    thread_pool,
                    project.project_directory(),
                    project.tintmap().as_path()?,
                    project.terrain_config().map_size(),
                    tint_clipmap.fullres_data_slice(),
                );
                commands.spawn().insert(task);
            }
            _ => {}
        }
    }
    Ok(())
}
// ----------------------------------------------------------------------------
pub(super) fn poll_async_store_map_tasks(
    mut commands: Commands,
    mut store_tasks: Query<(Entity, &mut StoreMapTask)>,
    mut task_finished: EventWriter<AsyncTaskFinishedEvent>,
    mut editor_events: EventWriter<EditorEvent>,
    mut progress: ResMut<AsyncTaskProgress>,
) -> Result<()> {
    use EditorEvent::ProgressTrackingUpdate;

    if !store_tasks.is_empty() {
        for (entity, mut task) in &mut store_tasks {
            if let Some(task_result) = future::block_on(future::poll_once(task.future_mut())) {
                // remove task component from entity
                commands.entity(entity).despawn();

                // check success
                task_result?;

                // update progress
                let maptype = task.maptype();

                editor_events.send(ProgressTrackingUpdate(
                    progress.update_processed(maptype, 1),
                ));
                if progress.is_finished(maptype) {
                    task_finished.send(maptype.finished_event());
                }
            }
        }
    }
    Ok(())
}
// ----------------------------------------------------------------------------
pub(super) fn watch_saving(
    mut app_state: ResMut<State<EditorState>>,
    mut project: ResMut<ProjectSettings>,
    mut tasks_finished: EventReader<AsyncTaskFinishedEvent>,
    mut editor_events: EventWriter<EditorEvent>,
) {
    use AsyncTaskFinishedEvent::{ProjectFullySaved, ProjectQuickSaved, TasksAborted};
    for task in tasks_finished.iter() {
        match task {
            ProjectFullySaved | ProjectQuickSaved => {
                info!("project saved.");
                project.reset_modified();
                app_state.overwrite_set(EditorState::Editing).ok();
            }
            TasksAborted(reason) => {
                error!("project saving failed {}", reason);
                editor_events.send(EditorEvent::ShowModalErrorSetState(
                    format!("Project saving failed:\n\n{}", reason),
                    EditorState::Editing,
                ))
            }
            _ => {}
        }
    }
}
// ----------------------------------------------------------------------------
pub(super) fn handle_save_error(
    In(result): In<Result<()>>,
    mut editor_events: EventWriter<EditorEvent>,
) {
    match result {
        Ok(_) => {}
        Err(reason) => {
            error!("project saving failed {}", reason);
            editor_events.send(EditorEvent::ProgressTrackingCancel);
            editor_events.send(EditorEvent::ShowModalErrorSetState(
                format!("Project saving failed:\n\n{}", reason),
                EditorState::Editing,
            ))
        }
    }
}
// ----------------------------------------------------------------------------
// watch editing to mark modified
// ----------------------------------------------------------------------------
pub(super) fn watch_editing(
    mut project: ResMut<ProjectSettings>,
    paint_events: EventReader<PaintingEvent>,
) {
    if !paint_events.is_empty() {
        project.set_modified_texturing();
    }
}
// ----------------------------------------------------------------------------
use std::path::PathBuf;

use bevy::prelude::*;
use bevy::tasks::{AsyncComputeTaskPool, Task};

use futures_lite::future;

use crate::cmds::{self, AsyncTaskFinishedEvent, AsyncTaskStartEvent};
use crate::project::{mapwriter, ProjectSettings};
use crate::terrain_clipmap::{HeightmapClipmap, TextureControlClipmap, TintClipmap};
use crate::terrain_painting::PaintingEvent;
use crate::terrain_render::TerrainMaterialSet;
use crate::{EditorEvent, EditorState};

use super::{AsyncTaskProgress, MapType, Result, StoreMapTask};
// ----------------------------------------------------------------------------
// task/progress helper
// ----------------------------------------------------------------------------
impl StoreMapTask {
    // ------------------------------------------------------------------------
    fn future_mut(&mut self) -> &mut Task<Result<PathBuf>> {
        &mut self.1
    }
    // ------------------------------------------------------------------------
    const fn maptype(&self) -> MapType {
        self.0
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl AsyncTaskProgress {
    // ------------------------------------------------------------------------
    fn reset(&mut self) -> &mut Self {
        self.0.clear();
        self
    }
    // ------------------------------------------------------------------------
    fn watch(&mut self, tasks: Vec<(MapType, usize)>) -> &mut Self {
        for (task, max) in tasks {
            self.0.insert(task, (0, max));
        }
        self
    }
    // ------------------------------------------------------------------------
    fn update_processed(&mut self, task: MapType, count: usize) -> cmds::TrackedProgress {
        if let Some(state) = self.0.get_mut(&task) {
            state.0 += count;
            task.progress_event(state.0, state.1)
        } else {
            cmds::TrackedProgress::Ignored
        }
    }
    // ------------------------------------------------------------------------
    fn is_finished(&self, task: MapType) -> bool {
        self.0
            .get(&task)
            .map(|(current, max)| current >= max)
            .unwrap_or(false)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl MapType {
    // ------------------------------------------------------------------------
    const fn progress_event(&self, current: usize, max: usize) -> cmds::TrackedProgress {
        use cmds::TrackedProgress::*;
        use MapType::*;

        match self {
            Texturing | TextureControl => SavedTextureMaps(current, max),
            Heightmap => SavedHeightmap(current >= max),
            Tintmap => SavedTintmap(current >= max),
        }
    }
    // ------------------------------------------------------------------------
    const fn finished_event(&self) -> cmds::AsyncTaskFinishedEvent {
        use cmds::AsyncTaskFinishedEvent::*;
        use MapType::*;

        match self {
            Texturing | TextureControl => TextureMapsSaved,
            Heightmap => HeightmapSaved,
            Tintmap => TintmapSaved,
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
