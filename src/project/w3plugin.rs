// ----------------------------------------------------------------------------
use std::path::{Path, PathBuf};

use bevy::prelude::*;
use witcher3::Witcher3HubId;

use super::MaterialSetRegistry;

use crate::project;
use crate::terrain_render::TerrainMaterialParam;
// ----------------------------------------------------------------------------
// systen
// ----------------------------------------------------------------------------
pub(super) fn setup_materialset_registry(
    w3conf: Res<witcher3::Witcher3Config>,
    mut registry: ResMut<MaterialSetRegistry>,
) {
    use Witcher3HubId::*;
    let w3conf = w3conf.terrain();

    for hub_id in [
        Prologue,
        PrologueWinter,
        Skellige,
        Novigrad,
        KaerMorhen,
        Vizima,
        IsleOfMist,
        SpiralDarkValley,
        Bob,
    ] {
        let materialset = (w3conf.texture_dir(), hub_id.materialset()).into();
        registry
            .sets
            .insert(hub_id.materialset_path().into(), materialset);
    }
}
// ----------------------------------------------------------------------------
impl From<(&Path, &Path, witcher3::HubSettings)> for project::ProjectSettings {
    // ------------------------------------------------------------------------
    fn from(
        (path_terrain_data, path_terrain_textures, hub): (&Path, &Path, witcher3::HubSettings),
    ) -> Self {
        use crate::loader::DataMapping::File;

        let mut settings = project::ProjectSettings::new(
            // cleanup name to [a..z_] charset
            &hub.id
                .chars()
                .map(|c| if c.is_ascii_alphabetic() { c } else { '_' })
                .collect::<String>()
                .to_lowercase(),
            path_terrain_data.join(format!("world.{}.yml", hub.name)),
            hub.map_size,
            hub.terrain_size,
            hub.min_height,
            hub.max_height,
            File(PathBuf::from(hub.heightmap_file)),
            File(PathBuf::from(hub.tintmap_file)),
            project::TextureMaps::new(
                File(PathBuf::from(hub.texture_background_file)),
                File(PathBuf::from(hub.texture_overlay_file)),
                File(PathBuf::from(hub.texture_control_file)),
            ),
            (path_terrain_textures, hub.materialset).into(),
            Some(hub.environment.to_string()),
            hub.clipmap_level,
        );
        // do not allow to quicksave in example dir
        settings.prevent_quicksave();
        settings
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl From<(&Path, witcher3::MaterialSet)> for project::MaterialSetConfig {
    // ------------------------------------------------------------------------
    fn from((base_path, materials): (&Path, witcher3::MaterialSet)) -> Self {
        project::MaterialSetConfig::new(
            materials.path,
            materials
                .diffuse
                .iter()
                .map(|p| base_path.join(p).display().to_string())
                .collect(),
            materials
                .normal
                .iter()
                .map(|p| base_path.join(p).display().to_string())
                .collect(),
            materials
                .parameter
                .iter()
                .map(TerrainMaterialParam::from)
                .collect(),
        )
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> From<&'a witcher3::TerrainMaterialParam> for TerrainMaterialParam {
    // ------------------------------------------------------------------------
    fn from(p: &witcher3::TerrainMaterialParam) -> Self {
        TerrainMaterialParam {
            blend_sharpness: p.blend_sharpness,
            slope_base_dampening: p.slope_base_dampening,
            slope_normal_dampening: p.slope_normal_dampening,
            specularity: p.specularity,
            specularity_base: p.specularity_base,
            specularity_scale: p.specularity_scale,
            falloff: p.falloff,
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
