// ----------------------------------------------------------------------------
#![forbid(unsafe_code)]
// #![cfg_attr(not(debug_assertions), deny(warnings))] // Forbid warnings in release builds

// disable console on windows for release builds
// #![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]
// ----------------------------------------------------------------------------
use getopts::{Matches, Options};
use std::env;
use std::path::PathBuf;

use winit::window::Icon;

use bevy::prelude::{App, ClearColor, Color, Msaa, NonSend, WindowDescriptor};
use bevy::window::WindowId;
use bevy::winit::WinitWindows;
use bevy::DefaultPlugins;

use bevy::render::settings::{WgpuFeatures, WgpuSettings};
use terrain_editor::EditorPlugin;
// ----------------------------------------------------------------------------
const VERSION: Option<&'static str> = option_env!("CARGO_PKG_VERSION");
const NAME: &str = "Terrain Texturing Editor";
// ----------------------------------------------------------------------------
struct CliArgs {
    #[cfg(feature = "witcher3")]
    game_dir: Option<PathBuf>,
    #[cfg(feature = "witcher3")]
    settings: Option<PathBuf>,
}
// ----------------------------------------------------------------------------
fn setup_option() -> Options {
    let mut opts = Options::new();

    // misc
    opts.optflag("h", "help", "print this help menu");

    #[cfg(feature = "witcher3")]
    {
        // directory options
        opts.optopt(
            "w",
            "witcher3-dir",
            "defines directory for vanilla witcher3 game installation.",
            "DIRECTORY",
        );
        // directory options
        opts.optopt(
            "s",
            "settings-file",
            "defines fullpath for settings file.",
            "FILE",
        );
    }

    opts
}
// ----------------------------------------------------------------------------
#[allow(dead_code)]
fn check_file(file: &str, errname: &str) -> Result<PathBuf, String> {
    let file = PathBuf::from(file);
    if !file.exists() || !file.is_file() {
        Err(format!("{} [{}] does not exist", errname, file.display()))
    } else {
        Ok(file)
    }
}
// ----------------------------------------------------------------------------
#[allow(dead_code)]
fn check_dir(dir: &str, name: &str) -> Result<PathBuf, String> {
    // check if dir exists
    let dir = PathBuf::from(dir);
    if !dir.exists() || !dir.is_dir() {
        Err(format!("{} [{}] does not exist", name, dir.display()))
    } else {
        Ok(dir)
    }
}
// ----------------------------------------------------------------------------
#[cfg(feature = "witcher3")]
fn parse_arguments(found: &Matches) -> Result<CliArgs, String> {
    let param_game_dir = found.opt_str("w");
    let param_settingsfile = found.opt_str("s");

    let game_dir = if let Some(game_dir) = param_game_dir {
        Some(check_dir(&game_dir, "game directory")?)
    } else {
        None
    };

    let settings = if let Some(file) = param_settingsfile {
        Some(check_file(&file, "settings file")?)
    } else {
        None
    };
    Ok(CliArgs { game_dir, settings })
}
// ----------------------------------------------------------------------------
#[cfg(not(feature = "witcher3"))]
fn parse_arguments(found: &Matches) -> Result<CliArgs, String> {
    Ok(CliArgs {})
}
// ----------------------------------------------------------------------------
fn print_usage(program: &str, opts: &Options) {
    let brief = format!(
        "{} v{} \nUsage: {} [options]",
        NAME,
        VERSION.unwrap_or("unknown"),
        program
    );
    print!("{}", opts.usage(&brief));
}
// ----------------------------------------------------------------------------
fn main() -> Result<(), String> {
    let mut app = App::new();
    println!("{} v{}", NAME, VERSION.unwrap_or("unknown"));

    app.insert_resource(ClearColor(Color::rgb(0.4, 0.4, 0.4)))
        // terrain rendering uses multiple rendertargets from witch subsequent
        // passes try to sample. therefore msaa must be deactivated.
        .insert_resource(Msaa { samples: 1 })
        .insert_resource(WgpuSettings {
            features: WgpuFeatures::TEXTURE_ADAPTER_SPECIFIC_FORMAT_FEATURES
                | WgpuFeatures::TEXTURE_FORMAT_16BIT_NORM,
            ..Default::default()
        })
        // reduce fps/power in unfocused/minimized mode
        .insert_resource(bevy::winit::WinitSettings {
            focused_mode: bevy::winit::UpdateMode::Continuous,
            unfocused_mode: bevy::winit::UpdateMode::ReactiveLowPower {
                max_wait: bevy::utils::Duration::from_millis(500),
            },
            ..Default::default()
        })
        // .insert_resource(bevy::log::LogSettings {
        //     level: bevy::log::Level::INFO,
        //     filter: "wgpu=error,bevy_render=trace".to_string(),
        // })
        .insert_resource(WindowDescriptor {
            width: 1200.,
            height: 800.,
            title: format!("{} v{}", NAME, VERSION.unwrap_or("unknown")),
            ..Default::default()
        })
        // pipelined default plugins initializes some lights???
        .add_plugins(DefaultPlugins)
        // .add_plugins_with(DefaultPlugins, |plugins| {
        //     plugins.disable::<LogPlugin>()
        // })
        .add_plugin(EditorPlugin)
        .add_startup_system(set_window_icon);

    #[cfg(debug_assertions)]
    {
        app
            // .add_plugin(bevy::diagnostic::FrameTimeDiagnosticsPlugin::default())
            .add_plugin(bevy::diagnostic::LogDiagnosticsPlugin::default());
    }

    // bevy_mod_debugdump::print_render_graph(&mut app);
    // bevy_mod_debugdump::print_render_schedule_graph(&mut app);
    // -------------------------------------------------------------------------
    let args: Vec<String> = env::args().collect();
    let program = args[0].clone();

    let opts = setup_option();

    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(f) => {
            print_usage(&program, &opts);
            return Err(f.to_string());
        }
    };

    if matches.opt_present("h") {
        print_usage(&program, &opts);
        return Ok(());
    }

    match parse_arguments(&matches) {
        Ok(args) => {
            #[cfg(feature = "witcher3")]
            {
                let mut w3conf = witcher3::load_config(args.settings)?;

                if let Some(game_dir) = args.game_dir {
                    w3conf.set_gamedir(&game_dir);
                }

                app.add_plugin(witcher3::Witcher3Plugin).insert_resource(
                    w3conf
                        .set_terrain_extraction_path("_w3-data_/terrain/")
                        .set_texture_extraction_path("_w3-data_/w3.textures/"),
                );
            }
            app.run();
            Ok(())
        }
        Err(msg) => {
            print_usage(&program, &opts);
            Err(msg)
        }
    }
}
// ----------------------------------------------------------------------------
fn set_window_icon(windows: NonSend<WinitWindows>) {
    let Some(primary) = windows.get_window(WindowId::primary()) else {
        return;
    };

    let (icon_rgba, icon_width, icon_height) = {
        let image = image::open("assets/logo-icon.png")
            .expect("Failed to open icon path")
            .into_rgba8();
        let (width, height) = image.dimensions();
        let rgba = image.into_raw();
        (rgba, width, height)
    };

    let icon = Icon::from_rgba(icon_rgba, icon_width, icon_height).unwrap();
    primary.set_window_icon(Some(icon));
}
// ----------------------------------------------------------------------------
