// ----------------------------------------------------------------------------
use std::path::PathBuf;

use bevy::{app::AppExit, prelude::*, render::render_resource::TextureFormat, tasks::Task};
use bevy_egui::EguiContext;
// ----------------------------------------------------------------------------
pub struct EditorPlugin;
// ----------------------------------------------------------------------------
use camera::CameraPlugin;

use cmds::AsyncTaskFinishedEvent;
use gui::UiImages;
use project::ProjectSettingsPlugin;

use crate::cmds::AsyncCmdsPlugin;
use crate::environment::EnvironmentPlugin;
use crate::heightmap::HeightmapPlugin;
use crate::terrain_clipmap::TerrainClipmapPlugin;
use crate::terrain_material::MaterialSetPlugin;
use crate::terrain_painting::TerrainPaintingPlugin;
use crate::terrain_render::TerrainShadowsComputePlugin;
use crate::terrain_tiles::TerrainTilesGeneratorPlugin;
// ----------------------------------------------------------------------------
mod atmosphere;
mod config;
mod loader;
mod project;

mod heightmap;
mod terrain_clipmap;
mod terrain_material;
mod terrain_tiles;
mod texturecontrol;
mod tintmap;

mod environment;
mod terrain_painting;
mod terrain_render;

mod camera;
mod clipmap;
mod compute;
mod mut_renderasset;
mod resource;
mod shapes;
mod texturearray;

mod cmds;
mod gui;

#[cfg(feature = "witcher3")]
mod w3plugin;
// ----------------------------------------------------------------------------
#[derive(Clone, Copy, Eq, PartialEq, Debug, Hash)]
enum EditorState {
    Initialization,
    NoTerrainData,
    TerrainLoading,
    Editing,
    ProjectSaving,
    FreeCam,
    ModalDialog,
    #[cfg(feature = "witcher3")]
    ExtractWitcher3Data,
}
// ----------------------------------------------------------------------------
/// events triggered by editor and not user (e.g. to update something in GUI)
enum EditorEvent {
    TerrainTextureUpdated(terrain_material::TextureUpdatedEvent),
    ProgressTrackingStart(cmds::TrackedTaskname, Vec<cmds::TrackedProgress>),
    ProgressTrackingUpdate(cmds::TrackedProgress),
    ProgressTrackingCancel,
    ShowModalError(String),
    ShowModalErrorSetState(String, EditorState),
    ToggleGuiVisibility,
    TriggerUiAction(gui::GuiAction),
    StateChange(EditorState),
    Debug(DebugEvent),
}
// ----------------------------------------------------------------------------
enum DebugEvent {
    ClipmapUpdate(String, u8, Handle<texturearray::TextureArray>),
}
// ----------------------------------------------------------------------------
#[derive(Default)]
struct DefaultResources {
    logo: Handle<Image>,
    placeholder_texture: Handle<Image>,
}
// ----------------------------------------------------------------------------
// sync loader of essential files
fn setup_default_assets(
    mut egui_ctx: ResMut<EguiContext>,
    mut ui_images: ResMut<UiImages>,
    mut resources: ResMut<DefaultResources>,
    mut textures: ResMut<Assets<Image>>,
) -> Result<(), String> {
    use bevy::render::render_resource::{Extent3d, TextureDimension};

    info!("startup_system: setup_default_assets");

    let logo_resolution = 150;
    let texture_resolution = 1024;

    let logo_data = loader::LoaderPlugin::load_png_data(
        png::ColorType::Rgba,
        png::BitDepth::Eight,
        logo_resolution,
        &PathBuf::from("assets/logo.png"),
    )?;

    let logo = Image::new(
        Extent3d {
            width: logo_resolution,
            height: logo_resolution,
            depth_or_array_layers: 1,
        },
        TextureDimension::D2,
        logo_data,
        TextureFormat::Rgba8UnormSrgb,
    );

    resources.logo = textures.add(logo);

    ui_images.set(&mut egui_ctx, "logo", resources.logo.clone_weak());

    // default material texture placeholder
    let default_texture_data = loader::LoaderPlugin::load_png_data(
        png::ColorType::Rgba,
        png::BitDepth::Eight,
        texture_resolution,
        &PathBuf::from("assets/placeholder_texture.png"),
    )?;

    resources.placeholder_texture = textures.add(Image::new(
        Extent3d {
            width: texture_resolution,
            height: texture_resolution,
            depth_or_array_layers: 1,
        },
        TextureDimension::D2,
        default_texture_data,
        TextureFormat::Rgba8UnormSrgb,
    ));

    info!("startup_system: setup_default_assets.done");
    Ok(())
}
// ----------------------------------------------------------------------------
fn handle_setup_errors(
    In(result): In<Result<(), String>>,
    mut app_exit_events: EventWriter<AppExit>,
) {
    match result {
        Ok(_) => {}
        Err(msg) => {
            error!("failed to initialize default resources. {}", msg);
            app_exit_events.send(AppExit);
        }
    }
}
// ----------------------------------------------------------------------------
fn finish_initialization(mut app_state: ResMut<State<EditorState>>) {
    app_state.overwrite_set(EditorState::NoTerrainData).unwrap();
}
// ----------------------------------------------------------------------------
fn signal_editor_state_change(
    app_state: Res<State<EditorState>>,
    mut editor_events: EventWriter<EditorEvent>,
) {
    editor_events.send(EditorEvent::StateChange(*app_state.current()));
}
// ----------------------------------------------------------------------------
#[derive(Component, Deref, DerefMut)]
struct TaskResult(Task<Result<TaskResultData, String>>);
// ----------------------------------------------------------------------------
enum TaskResultData {
    HeightmapData(heightmap::TerrainHeightMap),
    TextureControl(texturecontrol::TextureControl),
    TintMap(tintmap::TintMap),
}
// ----------------------------------------------------------------------------
fn init_terrain_loading(
    terrain_config: Res<config::TerrainConfig>,
    mut mesh_settings: ResMut<terrain_tiles::TerrainMeshSettings>,
    mut terrain_render_info: ResMut<terrain_render::TerrainMapInfo>,
) {
    // transfer settings required for rendering to dedicated render resource
    *terrain_render_info = terrain_render::TerrainMapInfo::from(terrain_config.as_ref());

    // auto setup some default lod count and error levels based on terrain size
    mesh_settings.setup_defaults_from_size(terrain_render_info.map_size);
}
// ----------------------------------------------------------------------------
fn start_terrain_loading(
    terrain_config: Res<config::TerrainConfig>,
    mut editor_events: EventWriter<EditorEvent>,
    mut task_manager: ResMut<cmds::AsyncCommandManager>,
) {
    // queue loading tasks
    task_manager.add_new(cmds::WaitForTerrainLoaded.into());
    task_manager.add_new(cmds::LoadHeightmap.into());
    task_manager.add_new(cmds::LoadTextureMap.into());
    task_manager.add_new(cmds::LoadTintMap.into());
    task_manager.add_new(cmds::LoadTerrainMaterialSet.into());

    // bigger terrains may take > 10s of loading. show a progress bar by tracking
    // all longer running events
    editor_events.send(EditorEvent::ProgressTrackingStart(
        "Loading Terrain".into(),
        vec![
            cmds::TrackedProgress::LoadHeightmap(false),
            cmds::TrackedProgress::LoadTextureMap(false),
            cmds::TrackedProgress::LoadTintMap(false),
            cmds::TrackedProgress::GeneratedHeightmapNormals(0, 1),
            cmds::TrackedProgress::GenerateTerrainTiles(false),
            cmds::TrackedProgress::GeneratedTerrainErrorMaps(0, terrain_config.tile_count()),
            cmds::TrackedProgress::GeneratedTerrainMeshes(0, terrain_config.tile_count()),
        ],
    ));
}
// ----------------------------------------------------------------------------
fn watch_loading(
    mut app_state: ResMut<State<EditorState>>,
    mut tasks_finished: EventReader<AsyncTaskFinishedEvent>,
    mut editor_events: EventWriter<EditorEvent>,
) {
    use AsyncTaskFinishedEvent::{TasksAborted, TerrainLoaded};
    for task in tasks_finished.iter() {
        match task {
            TerrainLoaded => {
                info!("terrain loaded.");
                app_state.overwrite_set(EditorState::Editing).ok();
            }
            TasksAborted(reason) => {
                error!("terrain loading failed: {}", reason);
                editor_events.send(EditorEvent::ShowModalErrorSetState(
                    format!("Terrain Loading aborted:\n\n{}", reason),
                    EditorState::NoTerrainData,
                ))
            }
            _ => {}
        }
    }
}
// ----------------------------------------------------------------------------
impl Plugin for EditorPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<DefaultResources>()
            .init_resource::<config::TerrainConfig>()
            .add_event::<EditorEvent>()
            .add_state(EditorState::Initialization)
            .add_plugin(project::ProjectSettingsPlugin)
            .add_plugin(compute::GpuComputeTaskPlugin)
            .add_plugin(cmds::AsyncCmdsPlugin)
            .add_plugin(texturearray::TextureArrayPlugin)
            .add_plugin(heightmap::HeightmapPlugin)
            .add_plugin(terrain_clipmap::TerrainClipmapPlugin)
            .add_plugin(terrain_material::MaterialSetPlugin)
            .add_plugin(terrain_tiles::TerrainTilesGeneratorPlugin)
            .add_plugin(terrain_render::TerrainRenderPlugin)
            .add_plugin(terrain_painting::TerrainPaintingPlugin)
            .insert_resource(camera::CameraSettings {
                rotation_sensitivity: 0.00015, // default: 0.00012
                movement_speed: 122.0,         // default: 12.0
                speed_modifier: 3.0,
            })
            .add_plugin(CameraPlugin)
            .add_plugin(gui::EditorUiPlugin)
            .insert_resource(atmosphere::AtmosphereMat::default())
            .add_plugin(atmosphere::AtmospherePlugin { dynamic: true })
            .add_plugin(environment::EnvironmentPlugin)
            .add_system(global_hotkeys);

        // --- state systems definition ---------------------------------------
        EditorState::initialization(app);
        EditorState::no_terrain_data(app);
        EditorState::terrain_loading(app);
        EditorState::terrain_editing(app);
        EditorState::free_cam(app);
        EditorState::modal_dialog(app);
        EditorState::project_saving(app);

        #[cfg(feature = "witcher3")]
        EditorState::extract_witcher3_data(app);
        // --- state systems definition END -----------------------------------
    }
}
// ----------------------------------------------------------------------------
impl EditorState {
    // ------------------------------------------------------------------------
    /// init of default resources/placeholders etc. with explicit ordering
    fn initialization(app: &mut App) {
        app.add_startup_system(
            setup_default_assets
                .chain(handle_setup_errors)
                .label("default_resources"),
        )
        .add_startup_system(
            terrain_material::setup_default_materialset
                .label("default_materialset")
                .after("default_resources"),
        )
        .add_startup_system(
            gui::initialize_ui
                .label("init_ui")
                .after("default_materialset"),
        )
        // there is no update phase in initialization, just transit to next state
        .add_startup_system(finish_initialization.after("init_ui"))
        // plugins
        .add_startup_system_set(EnvironmentPlugin::startup())
        .add_startup_system_set(ProjectSettingsPlugin::startup());
    }
    // ------------------------------------------------------------------------
    /// close project / unload terrain state
    fn no_terrain_data(app: &mut App) {
        use EditorState::NoTerrainData;

        app.add_system_set(
            SystemSet::on_enter(NoTerrainData).with_system(signal_editor_state_change),
        )
        .add_system_set(
            SystemSet::on_resume(NoTerrainData).with_system(signal_editor_state_change),
        );

        app // plugins
            .add_system_set(TerrainClipmapPlugin::reset_data(NoTerrainData))
            .add_system_set(TerrainShadowsComputePlugin::reset_data(NoTerrainData))
            .add_system_set(TerrainTilesGeneratorPlugin::reset_data(NoTerrainData))
            .add_system_set(MaterialSetPlugin::setup_default_materialset(NoTerrainData))
            .add_system_set(EnvironmentPlugin::activate_dynamic_updates(NoTerrainData))
            .add_system_set(EnvironmentPlugin::reset_data(NoTerrainData));
    }
    // ------------------------------------------------------------------------
    #[rustfmt::skip]
    /// load project / terrain data state
    fn terrain_loading(app: &mut App) {
        use EditorState::TerrainLoading;

        app.add_system_set(
            SystemSet::on_enter(TerrainLoading)
                .with_system(signal_editor_state_change)
                .with_system(init_terrain_loading).label("init_terrain_loading")
        )
        .add_system_set(SystemSet::on_enter(TerrainLoading)
            .with_system(start_terrain_loading).label("start_terrain_loading")
                .after("init_terrain_loading")
        )
        // clipmap tracker must be initialized with new config data
        // before loading starts
        .add_system_set(
            TerrainClipmapPlugin::init_tracker(TerrainLoading)
                .after("init_terrain_loading")
                .before("start_terrain_loading"),
        )
        // shadows compute must be inititialized with new config data
        // before loading starts (because lightheight clipmap must be initialized
        // before terrain clipmap is setup)
        .add_system_set(
            TerrainShadowsComputePlugin::init(TerrainLoading)
                .after("init_terrain_loading")
                .before("start_terrain_loading"),
        );

        app.add_system_set(SystemSet::on_resume(TerrainLoading).with_system(signal_editor_state_change));
        app.add_system_set(SystemSet::on_update(TerrainLoading).with_system(watch_loading));

        app // plugins
            .add_system_sets(AsyncCmdsPlugin::activate_async_cmds(TerrainLoading))
            .add_system_set(MaterialSetPlugin::terrain_material_loading(TerrainLoading))
            .add_system_set(HeightmapPlugin::generate_heightmap_normals(TerrainLoading))
            .add_system_set(TerrainTilesGeneratorPlugin::lazy_generation(TerrainLoading))
            .add_system_set(EnvironmentPlugin::setup_environment_settings(
                TerrainLoading,
            ));
    }
    // ------------------------------------------------------------------------
    /// main editing state
    fn terrain_editing(app: &mut App) {
        use EditorState::Editing;

        app.add_system_set(SystemSet::on_enter(Editing).with_system(signal_editor_state_change))
            .add_system_set(SystemSet::on_resume(Editing).with_system(signal_editor_state_change));

        app // plugins
            // required for triggering operations like mesh regeneration or reloading
            // parts of some other data without reloading complete terrain
            .add_system_sets(AsyncCmdsPlugin::activate_async_cmds(Editing))
            .add_system_set(EnvironmentPlugin::activate_dynamic_updates(Editing))
            .add_system_set(MaterialSetPlugin::terrain_material_loading(Editing))
            .add_system_set(TerrainClipmapPlugin::update_tracker(Editing))
            .add_system_set(TerrainTilesGeneratorPlugin::lazy_generation(Editing))
            .add_system_set(TerrainPaintingPlugin::process_brush_operations(Editing))
            .add_system_set(ProjectSettingsPlugin::watch_editing_operations(Editing));
    }
    // ------------------------------------------------------------------------
    /// project data saving state
    fn project_saving(app: &mut App) {
        use EditorState::ProjectSaving;

        app.add_system_set(
            SystemSet::on_enter(ProjectSaving).with_system(signal_editor_state_change),
        )
        .add_system_set(
            SystemSet::on_resume(ProjectSaving).with_system(signal_editor_state_change),
        );

        app // plugins
            // required for saving task tracking
            .add_system_sets(AsyncCmdsPlugin::activate_async_cmds(ProjectSaving))
            .add_system_sets(ProjectSettingsPlugin::setup_saving(ProjectSaving));
    }
    // ------------------------------------------------------------------------
    /// stacked state with active free cam (editing on hold)
    fn free_cam(app: &mut App) {
        use EditorState::FreeCam;

        app.add_system_set(SystemSet::on_enter(FreeCam).with_system(signal_editor_state_change))
            .add_system_set(SystemSet::on_resume(FreeCam).with_system(signal_editor_state_change))
            .add_system_set(
                SystemSet::on_enter(FreeCam).with_system(terrain_clipmap::enable_caching),
            );

        app // plugins
            .add_system_set(CameraPlugin::start_free_camera(FreeCam))
            .add_system_set(CameraPlugin::active_free_camera(FreeCam))
            .add_system_set(CameraPlugin::stop_free_camera(FreeCam))
            .add_system_set(EnvironmentPlugin::activate_dynamic_updates(FreeCam))
            .add_system_set(TerrainClipmapPlugin::update_tracker(FreeCam))
            .add_system_set(TerrainTilesGeneratorPlugin::lazy_generation(FreeCam));
    }
    // ------------------------------------------------------------------------
    fn modal_dialog(app: &mut App) {
        use EditorState::ModalDialog;

        app.add_system_set(
            SystemSet::on_enter(ModalDialog).with_system(signal_editor_state_change),
        )
        .add_system_set(SystemSet::on_resume(ModalDialog).with_system(signal_editor_state_change));
    }
    // ------------------------------------------------------------------------
    #[cfg(feature = "witcher3")]
    fn extract_witcher3_data(app: &mut App) {
        use witcher3::Witcher3Plugin;
        use EditorState::ExtractWitcher3Data;

        app.add_system_set(
            SystemSet::on_enter(ExtractWitcher3Data).with_system(signal_editor_state_change),
        )
        .add_system_set(
            SystemSet::on_resume(ExtractWitcher3Data).with_system(signal_editor_state_change),
        );

        app.add_system_set(
            SystemSet::on_update(ExtractWitcher3Data).with_system(w3plugin::watch_data_extraction),
        )
        // plugins
        .add_system_set(Witcher3Plugin::extract_data(ExtractWitcher3Data));
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
#[allow(clippy::single_match)]
fn global_hotkeys(
    keys: Res<Input<KeyCode>>,
    mut app_state: ResMut<State<EditorState>>,
    mut event: EventWriter<EditorEvent>,
) {
    use EditorState::*;

    for key in keys.get_just_pressed() {
        match app_state.current() {
            FreeCam => match key {
                KeyCode::F12 => event.send(EditorEvent::ToggleGuiVisibility),
                KeyCode::LControl => app_state.overwrite_pop().unwrap(),
                _ => {}
            },
            Editing => match key {
                KeyCode::F5 => event.send(EditorEvent::TriggerUiAction(
                    gui::GuiAction::QuickSaveProject,
                )),
                KeyCode::F12 => event.send(EditorEvent::ToggleGuiVisibility),
                KeyCode::LControl => app_state.overwrite_push(FreeCam).unwrap(),
                _ => (),
            },
            ProjectSaving => {}
            TerrainLoading => match key {
                KeyCode::F12 => event.send(EditorEvent::ToggleGuiVisibility),
                _ => (),
            },
            NoTerrainData => match key {
                KeyCode::F12 => event.send(EditorEvent::ToggleGuiVisibility),
                KeyCode::LControl => app_state.overwrite_push(FreeCam).unwrap(),
                _ => (),
            },
            ModalDialog => {}
            Initialization => {}
            #[cfg(feature = "witcher3")]
            ExtractWitcher3Data => {}
        }
    }
}
// ----------------------------------------------------------------------------
// helper
// ----------------------------------------------------------------------------
impl<'a> From<&'a config::TerrainConfig> for terrain_render::TerrainMapInfo {
    // ------------------------------------------------------------------------
    fn from(config: &'a config::TerrainConfig) -> Self {
        Self {
            map_size: config.map_size(),
            resolution: config.resolution(),
            height_min: config.min_height(),
            height_max: config.max_height(),
            clipmap_level_count: config.clipmap_levels(),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
trait AppHelper {
    fn add_system_sets(&mut self, system_sets: Vec<SystemSet>) -> &mut Self;
}
// ----------------------------------------------------------------------------
impl AppHelper for App {
    // ------------------------------------------------------------------------
    fn add_system_sets(&mut self, system_sets: Vec<SystemSet>) -> &mut Self {
        for system_set in system_sets {
            self.add_system_set(system_set);
        }
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
