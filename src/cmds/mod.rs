//
// cmds are per "complex" operation that may involve multiple (complicated)
// sub tasks and/or be async
//
// ----------------------------------------------------------------------------
// Note: the order of the derive and enum_dispatch is important: setting it
// *after* hash + eq ensures only distinc tasks are queued (payload like path
// is ignored - which is what is desired here)
#[derive(Hash, PartialEq, Eq)]
#[enum_dispatch]
#[derive(Debug)]
pub enum AsyncTask {
    LoadHeightmap,
    LoadTextureMap,
    LoadTintMap,
    GenerateClipmap,
    GenerateHeightmapNormals,
    GenerateTerrainTiles,
    GenerateTerrainMeshErrorMaps,
    GenerateTerrainMeshes,
    LoadTerrainMaterialSet,
    WaitForTerrainLoaded,
    SaveTextureMaps,
    SaveHeightmap,
    SaveTintmap,
    WaitForProjectQuickSaved,
    WaitForProjectFullySaved,
}
// ----------------------------------------------------------------------------
pub use self::progress::{TrackedProgress, TrackedTaskname};
// ----------------------------------------------------------------------------
#[derive(Debug, Copy, Clone)]
pub enum AsyncTaskStartEvent {
    LoadHeightmap,
    LoadTextureMap,
    LoadTintMap,
    GenerateClipmap,
    GenerateHeightmapNormals,
    GenerateTerrainTiles,
    GenerateTerrainMeshErrorMaps,
    GenerateTerrainMeshes,
    LoadTerrainMaterialSet,
    WaitForTerrainLoaded,
    SaveTextureMaps,
    SaveHeightmap,
    SaveTintmap,
    WaitForProjectQuickSaved,
    WaitForProjectFullySaved,
}
// ----------------------------------------------------------------------------
#[derive(Hash, PartialEq, Eq, Clone, Debug)]
pub enum AsyncTaskFinishedEvent {
    HeightmapLoaded,
    TextureMapLoaded,
    TintMapLoaded,
    ClipmapGenerated,
    HeightmapNormalsGenerated,
    TerrainTilesGenerated,
    TerrainMeshErrorMapsGenerated,
    TerrainMeshesGenerated,
    TerrainLoaded,
    TerrainMaterialSetLoaded,
    TextureMapsSaved,
    HeightmapSaved,
    TintmapSaved,
    ProjectQuickSaved,
    ProjectFullySaved,
    TasksAborted(String),
}
// ----------------------------------------------------------------------------
#[derive(Debug, Default)]
pub struct LoadHeightmap;
// ----------------------------------------------------------------------------
#[derive(Debug, Default)]
pub struct LoadTextureMap;
// ----------------------------------------------------------------------------
#[derive(Debug, Default)]
pub struct LoadTintMap;
// ----------------------------------------------------------------------------
#[derive(Debug, Default)]
pub struct GenerateHeightmapNormals;
// ----------------------------------------------------------------------------
#[derive(Debug, Default)]
pub struct GenerateTerrainTiles;
// ----------------------------------------------------------------------------
#[derive(Debug, Default)]
pub struct GenerateTerrainMeshErrorMaps;
// ----------------------------------------------------------------------------
#[derive(Debug, Default)]
pub struct GenerateTerrainMeshes;
// ----------------------------------------------------------------------------
#[derive(Debug, Default)]
pub struct GenerateClipmap;
// ----------------------------------------------------------------------------
#[derive(Debug, Default)]
pub struct LoadTerrainMaterialSet;
// ----------------------------------------------------------------------------
#[derive(Debug, Default)]
pub struct WaitForTerrainLoaded;
// ----------------------------------------------------------------------------
#[derive(Debug, Default)]
pub struct SaveTextureMaps;
// ----------------------------------------------------------------------------
#[derive(Debug, Default)]
pub struct SaveHeightmap;
// ----------------------------------------------------------------------------
#[derive(Debug, Default)]
pub struct SaveTintmap;
// ----------------------------------------------------------------------------
#[derive(Debug, Default)]
pub struct WaitForProjectQuickSaved;
// ----------------------------------------------------------------------------
#[derive(Debug, Default)]
pub struct WaitForProjectFullySaved;
// ----------------------------------------------------------------------------
// systems
// ----------------------------------------------------------------------------
pub(crate) use async_cmds::AsyncCmdsPlugin;
pub(crate) use async_cmds::AsyncCommandManager;

pub(crate) use progress::TrackedProgressApi;
// ----------------------------------------------------------------------------
mod async_cmds;
mod progress;
// ----------------------------------------------------------------------------
use enum_dispatch::enum_dispatch;
// ----------------------------------------------------------------------------
