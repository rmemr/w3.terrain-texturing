// ----------------------------------------------------------------------------
use std::path;

use bevy::app;
use bevy::{math::Vec3, prelude::*};
use bevy_egui::EguiContext;

use crate::atmosphere::AtmosphereMat;
use crate::cmds as tasks;
use crate::config;
use crate::environment::{DayNightCycle, EnvironmentConfigRegistry, SunPositionSettings};
use crate::project;
use crate::terrain_material::{TerrainMaterialSet, TextureType, TextureUpdatedEvent};
use crate::terrain_tiles::{LodSlot, TerrainMeshSettings};
use crate::texturearray::TextureArray;
use crate::{EditorEvent, EditorState};
// ----------------------------------------------------------------------------
pub struct EditorUiPlugin;
// ----------------------------------------------------------------------------
pub use self::images::UiImages;
// ----------------------------------------------------------------------------
#[derive(Default)]
pub struct UiState {
    fullscreen: bool,
    enabled: bool,
    project_open: bool,
    project_is_loading: bool,

    progress: ProgressTracking,

    toolbox: toolbox::ToolboxState,

    is_using_pointer: bool,
    wants_pointer: bool,
    wants_keyboard: bool,

    // modal dialogs
    modal: UiModalDialogs,

    // debug
    debug: debug::DebugUiState,
}
// ----------------------------------------------------------------------------
// Modal dialogs
// ----------------------------------------------------------------------------
#[derive(Debug, Clone)]
pub enum ModalDialog {
    LoadedProjectSettings,
    ShowErrorPopup(String),
}
// ----------------------------------------------------------------------------
#[derive(Default)]
struct UiModalDialogs {
    open: Vec<ModalDialog>,
    exit_state: Option<EditorState>,
}
// ----------------------------------------------------------------------------
#[derive(Debug, Clone)]
/// Events triggered by user in the GUI (user actions)
pub enum GuiAction {
    // select directory, show settings as modal dialog,
    OpenProject(path::PathBuf),
    QuickSaveProject,
    SaveProjectIn(path::PathBuf),
    CloseProject,

    Toolbox(toolbox::ToolboxAction),
    UpdateDayNightCycleSetting(DayNightCycleSetting),
    UpdateSunSetting(SunSetting),
    UpdateAtmosphereSetting(AtmosphereSetting),
    UpdateMeshSetting(MeshSetting),
    UpdateRenderSettings(RenderSetting),
    LoadTerrain(Box<project::ProjectSettings>),
    OpenModalDialog(ModalDialog),
    CloseModalDialog,
    QuitRequest,
    DebugShowClipmap(bool),
    #[cfg(feature = "witcher3")]
    Witcher3Plugin(w3plugin::GuiAction),
}
// ----------------------------------------------------------------------------
#[derive(Debug, Clone)]
pub enum DayNightCycleSetting {
    SetTimeOfDay(f32),
    SetCycleSpeed(u16),
}
// ----------------------------------------------------------------------------
#[derive(Debug, Clone)]
pub enum SunSetting {
    SetPlaneTilt(u16),
    SetPlaneYaw(u16),
    SetPlaneHeight(u16),
    ToggleDebugMesh,
}
// ----------------------------------------------------------------------------
#[derive(Debug, Clone)]
pub enum AtmosphereSetting {
    SetRayOrigin(Vec3),
    SetSunIntensity(f32),
    SetPlanetRadius(f32),
    SetAtmosphereRadius(f32),
    SetRayleighScattering(Vec3),
    SetRayleighScaleHeight(f32),
    SetMieScattering(f32),
    SetMieScaleHeight(f32),
    SetMieScatteringDirection(f32),
    ResetToDefault,
}
// ----------------------------------------------------------------------------
#[derive(Debug, Clone)]
pub enum MeshSetting {
    SetLodCount(u8),
    SetLodMinError(f32),
    SetLodMaxError(f32),
    SetLodMaxDistance(f32),
    SetLodError(LodSlot, f32),
    SetLodDistance(LodSlot, f32),
    FreezeLods,
    ResetToDefault,
}
// ----------------------------------------------------------------------------
#[derive(Debug, Clone)]
// Note: as of now other settings are directly flipped
pub enum RenderSetting {
    OverlayWireframe(bool),
}
// ----------------------------------------------------------------------------
use self::egui_extensions::UiExtension;

use self::progresstracking::ProgressTracking;
// ----------------------------------------------------------------------------
mod debug;

mod toolbox;

mod cmds;
mod images;
mod progresstracking;
mod update;
mod view;

mod egui_extensions;

#[cfg(feature = "witcher3")]
mod w3plugin;
// ----------------------------------------------------------------------------
impl Plugin for EditorUiPlugin {
    // ------------------------------------------------------------------------
    fn build(&self, app: &mut App) {
        app.add_plugin(bevy_egui::EguiPlugin)
            .add_plugin(debug::EditorUiDebugPlugin)
            .add_plugin(toolbox::TexturingToolboxPlugin)
            .init_resource::<UiState>()
            .init_resource::<UiImages>()
            .add_event::<GuiAction>()
            .add_system(view::show_ui.label("ui").label("gui_actions"))
            .add_system(
                view::show_modal_ui
                    .label("modal.ui")
                    .label("gui_actions")
                    // make sure ui systems are not parallelized to prevent
                    // races (e.g remaining draw area sizes)
                    .after("ui"),
            )
            .add_system(update_input_processing_request.after("gui_actions"))
            .add_system(handle_editor_events)
            .add_system(log_ui_actions.after("gui_actions"))
            .add_system(
                handle_ui_actions
                    .chain(handle_action_result)
                    .label("handle_ui_actions")
                    .after("gui_actions"),
            );
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
fn handle_action_result(
    In(result): In<Result<Option<GuiAction>, String>>,
    mut gui_event: EventWriter<GuiAction>,
) {
    match result {
        Ok(None) => {}
        Ok(Some(subsequent_action)) => {
            gui_event.send(subsequent_action);
        }
        Err(msg) => {
            error!("error {}", msg);
            gui_event.send(GuiAction::OpenModalDialog(ModalDialog::ShowErrorPopup(msg)))
        }
    }
}
// ----------------------------------------------------------------------------
const TEXTURE_PREVIEW_SIZE_SMALL: u32 = 64;
// ----------------------------------------------------------------------------
pub(super) fn initialize_ui(
    mut egui_ctx: ResMut<EguiContext>,
    mut egui_image_registry: ResMut<UiImages>,
    mut images: ResMut<Assets<Image>>,
    texture_arrays: ResMut<Assets<TextureArray>>,
    materialset: ResMut<TerrainMaterialSet>,
) {
    info!("startup_system: initialize_ui");

    // setup egui link to terrain texture preview images
    for (array_handle, texture_type) in [
        (&materialset.diffuse, TextureType::Diffuse),
        (&materialset.normal, TextureType::Normal),
    ] {
        if let Some(array) = texture_arrays.get(array_handle) {
            for i in 0..array.texture_count() {
                let (format, size, img_data) = array.imagedata(i as u8, TEXTURE_PREVIEW_SIZE_SMALL);

                egui_image_registry.add_image(
                    &mut egui_ctx,
                    &mut images,
                    format!("terraintexture.{}.{}", texture_type, i),
                    format,
                    (size, size),
                    img_data,
                );
            }
        }

        // if let Some(array) = texture_arrays.get(&materialset.normal) {
        //     for i in 0..array.texture_count() {
        //         let (format, size, img_data) = array.imagedata(i as u8, TEXTURE_PREVIEW_SIZE_SMALL);

        //         egui_image_registry.add_image(
        //             &mut egui_ctx,
        //             &mut *images,
        //             format!("terraintexture.{}.{}", TextureType::Normal, i),
        //             format,
        //             (size, size),
        //             img_data,
        //         );
        //     }
        // }
    }
}
// ----------------------------------------------------------------------------
#[allow(clippy::too_many_arguments)]
fn handle_editor_events(
    mut ui_state: ResMut<UiState>,
    mut app_state: ResMut<State<EditorState>>,
    mut egui_image_registry: ResMut<UiImages>,
    mut images: ResMut<Assets<Image>>,
    texture_arrays: Res<Assets<TextureArray>>,
    materialset: Res<TerrainMaterialSet>,
    mut events: EventReader<EditorEvent>,
    mut ui_action: EventWriter<GuiAction>,
) {
    use EditorEvent::*;
    use ModalDialog::ShowErrorPopup;

    for event in events.iter() {
        match event {
            TerrainTextureUpdated(TextureUpdatedEvent(slot, texture_ty)) => {
                let handle = match texture_ty {
                    TextureType::Diffuse => &materialset.diffuse,
                    // TODO ignore normal?
                    TextureType::Normal => &materialset.normal,
                };

                if let Some(array) = texture_arrays.get(handle) {
                    let (_, _, img_data) = array.imagedata(**slot, TEXTURE_PREVIEW_SIZE_SMALL);
                    egui_image_registry.update_image(
                        &mut images,
                        &format!("terraintexture.{}.{}", texture_ty, slot),
                        img_data,
                    );
                }
            }
            ProgressTrackingStart(name, subtasks) => {
                ui_state.progress.start_task_tracking(name, subtasks);
            }
            ProgressTrackingUpdate(update) => {
                ui_state.progress.update(update);
            }
            ProgressTrackingCancel => {
                ui_state.progress.cancel_tracking();
            }
            StateChange(new_state) => {
                ui_state.update(*new_state);
            }
            ToggleGuiVisibility => {
                ui_state.fullscreen = !ui_state.fullscreen;
            }
            TriggerUiAction(action) => ui_action.send(action.clone()),
            ShowModalError(error) => {
                update::open_modal_dialog(
                    &mut app_state,
                    &mut ui_state,
                    ShowErrorPopup(error.to_owned()),
                );
            }
            ShowModalErrorSetState(error, new_state) => {
                update::open_modal_dialog_with_exit_state(
                    &mut app_state,
                    &mut ui_state,
                    *new_state,
                    ShowErrorPopup(error.to_owned()),
                );
            }

            Debug(_) => {}
        }
    }
}
// ----------------------------------------------------------------------------
#[allow(clippy::too_many_arguments)]
fn handle_ui_actions(
    mut commands: Commands,
    mut ui_action: EventReader<GuiAction>,
    mut ui_state: ResMut<UiState>,
    mut app_state: ResMut<State<EditorState>>,
    mut app_exit: EventWriter<app::AppExit>,
    mut project: ResMut<project::ProjectSettings>,
    materialset_registry: Res<project::MaterialSetRegistry>,
    env_registry: Res<EnvironmentConfigRegistry>,
    mut terrain_config: ResMut<config::TerrainConfig>,
    mut daylight_cycle: ResMut<DayNightCycle>,
    mut sun_settings: Option<ResMut<SunPositionSettings>>,
    mut atmosphere_settings: Option<ResMut<AtmosphereMat>>,
    mut mesh_settings: Option<ResMut<TerrainMeshSettings>>,
    mut task_manager: ResMut<tasks::AsyncCommandManager>,
    #[cfg(feature = "witcher3")] mut w3conf: ResMut<witcher3::Witcher3Config>,
) -> Result<Option<GuiAction>, String> {
    for action in ui_action.iter() {
        match action {
            GuiAction::Toolbox(_action) => {
                // handled by toolbox module explicitely
                // this would be the place for a reactive one shot system
            }
            GuiAction::OpenModalDialog(dialog) => {
                update::open_modal_dialog(&mut app_state, &mut ui_state, dialog.clone());
            }
            GuiAction::CloseModalDialog => {
                update::close_modal_dialog(&mut app_state, &mut ui_state);
            }
            GuiAction::QuitRequest => {
                // TODO if project modified -> popup
                app_exit.send(app::AppExit);
            }
            GuiAction::UpdateDayNightCycleSetting(setting) => {
                update::update_daynight_cycle_settings(setting, &mut daylight_cycle)
            }
            GuiAction::UpdateSunSetting(setting) => {
                update::update_sun_settings(setting, &mut sun_settings)
            }
            GuiAction::UpdateAtmosphereSetting(setting) => {
                update::update_atmosphere_settings(setting, &mut atmosphere_settings)
            }
            GuiAction::UpdateMeshSetting(setting) => {
                update::update_mesh_settings(setting, &mut mesh_settings)
            }
            GuiAction::UpdateRenderSettings(setting) => {
                update::update_render_settings(setting, &mut task_manager)
            }
            GuiAction::OpenProject(file) => {
                // load project and open project settings dialog
                return cmds::open_project(
                    file,
                    &mut project,
                    &env_registry,
                    &materialset_registry,
                )
                .map(Some);
            }
            GuiAction::QuickSaveProject => {
                return cmds::start_quick_saving_project(&mut app_state, &mut project, false)
            }
            GuiAction::SaveProjectIn(directory) => {
                return cmds::start_saving_project_in(&mut app_state, &mut project, directory)
            }
            GuiAction::CloseProject => {
                cmds::close_project(&mut app_state);
            }
            GuiAction::LoadTerrain(new_project) => update::start_terrain_loading(
                &mut app_state,
                &mut project,
                &mut terrain_config,
                new_project,
            ),
            // TODO should be removed later
            GuiAction::DebugShowClipmap(_) => {}

            #[cfg(feature = "witcher3")]
            GuiAction::Witcher3Plugin(action) => {
                w3plugin::handle_witcher3_plugin_action(
                    &mut w3conf,
                    action,
                    &mut commands,
                    &mut app_state,
                    &mut project,
                    &mut terrain_config,
                );
            }
        }
    }
    Ok(None)
}
// ----------------------------------------------------------------------------
impl UiState {
    // ------------------------------------------------------------------------
    fn update(&mut self, editor_state: EditorState) {
        match editor_state {
            EditorState::Initialization => {}
            EditorState::ModalDialog => {
                self.enabled = false;
            }
            EditorState::NoTerrainData => {
                self.enabled = true;
                self.project_is_loading = false;
                self.project_open = false;
                self.toolbox.enabled = false;
            }
            EditorState::TerrainLoading => {
                self.enabled = true;
                self.toolbox.enabled = false;
                self.project_open = false;
                self.project_is_loading = true;
            }
            EditorState::ProjectSaving => {
                self.enabled = false;
            }
            EditorState::Editing => {
                self.enabled = true;
                self.toolbox.enabled = true;
                self.project_open = true;
                self.project_is_loading = false;
            }
            EditorState::FreeCam => {
                self.enabled = false;
                self.toolbox.enabled = false;
            }
            #[cfg(feature = "witcher3")]
            EditorState::ExtractWitcher3Data => {
                self.enabled = true;
                self.project_is_loading = true;
                self.project_open = false;
                self.toolbox.enabled = false;
            }
        }
    }
    // ------------------------------------------------------------------------
    #[inline(always)]
    pub fn wants_input(&self) -> bool {
        self.wants_pointer || self.wants_keyboard || self.is_using_pointer
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl UiModalDialogs {
    // ------------------------------------------------------------------------
    fn open(&mut self, dialog: ModalDialog) {
        self.open.push(dialog);
    }
    // ------------------------------------------------------------------------
    fn close(&mut self) -> bool {
        self.open.pop();
        self.open.is_empty()
    }
    // ------------------------------------------------------------------------
    fn set_exit_state(&mut self, exit_state: EditorState) {
        self.exit_state = Some(exit_state);
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
fn update_input_processing_request(
    mut egui_ctx: ResMut<EguiContext>,
    mut ui_state: ResMut<UiState>,
) {
    let is_using_pointer = egui_ctx.ctx_mut().is_using_pointer();
    let wants_pointer = egui_ctx.ctx_mut().wants_pointer_input();
    let wants_keyboard = egui_ctx.ctx_mut().wants_keyboard_input();

    if ui_state.wants_pointer != wants_pointer
        || ui_state.wants_keyboard != wants_keyboard
        || ui_state.is_using_pointer != is_using_pointer
    {
        ui_state.wants_pointer = wants_pointer;
        ui_state.wants_keyboard = wants_keyboard;
        ui_state.is_using_pointer = is_using_pointer;
    }
}
// ----------------------------------------------------------------------------
// debug
// ----------------------------------------------------------------------------
#[allow(dead_code)]
fn log_ui_actions(mut ui_action: EventReader<GuiAction>) {
    for ev in ui_action.iter() {
        debug!("UI Action {:?}", ev);
    }
}
// ----------------------------------------------------------------------------
