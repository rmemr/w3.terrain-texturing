// ----------------------------------------------------------------------------
#[inline]
pub(super) fn show_opened_settings(
    egui_ctx: &mut EguiContext,
    is_top_window: bool,
    settings: &mut project::ProjectSettings,
    gui_event: &mut EventWriter<GuiAction>,
) {
    use ValidatedSetting::*;

    let mut is_opened = true;
    egui::Window::new("Open Project...")
        .collapsible(false)
        .resizable(false)
        .fixed_size((550.0, 300.0))
        .open(&mut is_opened)
        .anchor(Align2::CENTER_CENTER, egui::vec2(0.0, -100.0))
        .enabled(is_top_window)
        .show(egui_ctx.ctx_mut(), |ui| {
            let s = settings;
            let t = s.terrain_config();
            const LABEL_WIDTH: f32 = 100.0;

            egui::Grid::new("project.settings.basic")
                .num_columns(2)
                .min_col_width(LABEL_WIDTH)
                .show(ui, |ui| {
                    // Name
                    ui.label("Name:");
                    ui.validated_label(s.name(), s.validation_error(WorldId));
                    ui.end_row();

                    ui.label("Definition File:");
                    ui.label(s.world_definition_file().display().to_string());
                    ui.end_row();

                    ui.label("Project Directory:");
                    ui.label(s.project_directory().display().to_string());
                    ui.end_row();
                });

            ui.separator();
            egui::Grid::new("project.settings.terrain")
                .num_columns(2)
                .min_col_width(LABEL_WIDTH)
                .show(ui, |ui| {
                    ui.small("Terrain");
                    ui.end_row();

                    ui.label("  Map Size:");
                    ui.validated_label(
                        format!("{} x {}", t.map_size(), t.map_size()),
                        s.validation_error(MapSize),
                    );
                    ui.end_row();

                    ui.label("  Terrain Size:");
                    ui.validated_label(
                        format!("{} m x {} m", t.terrain_size(), t.terrain_size()),
                        s.validation_error(TerrainSize),
                    );
                    ui.end_row();

                    ui.label("  Terrain Height:");
                    ui.validated_label(
                        format!("{} m   -   {} m", t.min_height(), t.max_height()),
                        s.validation_error(TerrainHeight),
                    );
                    ui.end_row();
                });

            ui.separator();
            egui::Grid::new("project.settings.maps")
                .num_columns(2)
                .min_col_width(LABEL_WIDTH)
                .show(ui, |ui| {
                    ui.small("Terrain-Maps");
                    ui.end_row();

                    ui.label("  Heightmap:");
                    ui.validated_label(s.heightmap().display(), s.validation_error(Heightmap));
                    ui.end_row();

                    ui.label("  Tintmap:");
                    ui.validated_label(s.tintmap().display(), s.validation_error(Tintmap));
                    ui.end_row();

                    ui.small("Texturing");
                    ui.end_row();

                    ui.label("  Overlay:");
                    ui.validated_label(
                        s.texturemaps().overlay().display(),
                        s.validation_error(TexturingOverlay),
                    );
                    ui.end_row();

                    ui.label("  Background:");
                    ui.validated_label(
                        s.texturemaps().background().display(),
                        s.validation_error(TexturingBackground),
                    );
                    ui.end_row();

                    ui.label("  Blendcontrol:");
                    ui.validated_label(
                        s.texturemaps().blendcontrol().display(),
                        s.validation_error(TexturingControl),
                    );
                    ui.end_row();

                    ui.label("  Materialset:");
                    ui.label(s.materialset().id());
                });

            ui.separator();
            egui::Grid::new("project.settings.misc")
                .num_columns(2)
                .min_col_width(LABEL_WIDTH)
                .show(ui, |ui| {
                    ui.small("Rendering");
                    ui.end_row();

                    ui.label("  Environment:");
                    ui.label(s.environment_definition().unwrap_or("default"))
                });

            ui.separator();
            ui.columns(2, |col| {
                use GuiAction::*;
                col[0].vertical_centered(|ui| {
                    if ui.button("  Cancel  ").clicked() {
                        gui_event.send(CloseModalDialog);
                    }
                });

                col[1].vertical_centered(|ui| {
                    if ui
                        .add_enabled(s.is_valid(), egui::Button::new("   Load   "))
                        .clicked()
                    {
                        gui_event.send(CloseModalDialog);
                        gui_event.send(LoadTerrain(Box::new(s.clone())));
                    }
                });
            });
        });

    if !is_opened {
        gui_event.send(GuiAction::CloseModalDialog);
    }
}
// ----------------------------------------------------------------------------
use bevy::prelude::*;
use bevy_egui::{
    egui::{self, Align2},
    EguiContext,
};

use crate::project::{self, ValidatedSetting};

use super::{GuiAction, UiExtension};
// ----------------------------------------------------------------------------
