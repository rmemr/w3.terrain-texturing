// ----------------------------------------------------------------------------
#[inline]
pub(super) fn show_error_popup(
    egui_ctx: &mut EguiContext,
    is_top_window: bool,
    msg: &str,
    gui_event: &mut EventWriter<GuiAction>,
) {
    let mut is_opened = true;

    egui::Window::new("⚠   Error   ⚠")
        .collapsible(false)
        .resizable(false)
        .min_width(400.0)
        .default_height(150.0)
        .open(&mut is_opened)
        .anchor(Align2::CENTER_CENTER, egui::vec2(0.0, -100.0))
        .enabled(is_top_window)
        .show(egui_ctx.ctx_mut(), |ui| {
            ui.label(msg);

            ui.with_layout(egui::Layout::bottom_up(egui::Align::Center), |ui| {
                if ui.button("  Ok  ").clicked() {
                    gui_event.send(GuiAction::CloseModalDialog);
                }
                ui.separator();
            });
        });

    if !is_opened {
        gui_event.send(GuiAction::CloseModalDialog);
    }
}
// ----------------------------------------------------------------------------
use bevy::prelude::EventWriter;
use bevy_egui::{
    egui::{self, Align2},
    EguiContext,
};

use super::GuiAction;
// ----------------------------------------------------------------------------
