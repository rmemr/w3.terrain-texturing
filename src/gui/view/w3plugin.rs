// ----------------------------------------------------------------------------
#[inline]
pub(super) fn show_menu(
    ui: &mut egui::Ui,
    ui_state: &UiState,
    w3conf: &witcher3::Witcher3Config,
    gui_event: &mut EventWriter<GuiAction>,
) {
    use super::super::w3plugin::GuiAction::*;
    use witcher3::Witcher3HubId::*;

    ui.set_enabled(!ui_state.project_is_loading);

    ui.add_space(50.0);
    ui.separator();

    ui.menu_button("🐺 Witcher 3", |ui| {
        use super::GuiAction::Witcher3Plugin;

        let mut result = None;

        ui.set_enabled(!ui_state.project_open);

        if w3conf.is_valid_gamedir() {
            ui.menu_button("⛏ Extract...", |ui| {
                ui.menu_button("🌍 Terraindata...", |ui| {
                    let all_hubs = [
                        Prologue,
                        PrologueWinter,
                        Skellige,
                        Novigrad,
                        KaerMorhen,
                        Vizima,
                        IsleOfMist,
                        SpiralDesert,
                        Bob,
                    ];
                    for hub in all_hubs {
                        if ui.button(hub.short_caption()).clicked() {
                            result = Some(Witcher3Plugin(ExtractTerrainData(vec![hub])));
                        }
                    }

                    ui.separator();
                    if ui.button("🌍 All Hubs").clicked() {
                        result = Some(Witcher3Plugin(ExtractTerrainData(all_hubs.to_vec())));
                    }
                });

                ui.separator();
                ui.menu_button("🎨 Terrain Textures...", |ui| {
                    // Note: some hubs reuse textures from other hubs
                    let all_hubs = [
                        Prologue,
                        PrologueWinter,
                        Skellige,
                        Novigrad,
                        KaerMorhen,
                        SpiralDesert,
                        Bob,
                    ];
                    for hub in all_hubs {
                        if ui.button(hub.textureset_caption()).clicked() {
                            result = Some(Witcher3Plugin(ExtractTerrainTextures(vec![hub])));
                        }
                    }

                    ui.separator();
                    if ui.button("🎨 All Hubs").clicked() {
                        result = Some(Witcher3Plugin(ExtractTerrainTextures(all_hubs.to_vec())));
                    }
                });
            });
        } else if ui.button("⛏ Find Game Directory...").clicked() {
            if let Ok(path) = pick_game_dir() {
                result = Some(Witcher3Plugin(SetGamePath(path)));
            }
        }
        ui.separator();

        for hub in w3conf.terrain().available_hubdata() {
            if ui.button(hub.caption()).clicked() {
                result = Some(Witcher3Plugin(LoadHub(*hub)));
            }
        }

        if let Some(event) = result {
            ui.close_menu();
            gui_event.send(event);
        }
    });
}
// ----------------------------------------------------------------------------
use std::path::PathBuf;

use bevy::{log, prelude::EventWriter};
use bevy_egui::egui;

use super::{GuiAction, UiState};
// ----------------------------------------------------------------------------
fn pick_game_dir() -> Result<PathBuf, String> {
    let mut picked_path = PathBuf::default();

    while let Err(msg) = witcher3::Witcher3Config::validate_game_dir(&picked_path) {
        if !msg.is_empty() {
            log::error!("failed to validate witcher 3 game dir: {}", msg);
        }

        if let Some(directory) = rfd::FileDialog::new()
            .set_title("Select The Witcher 3 game directory")
            .pick_folder()
        {
            picked_path = directory;
        } else {
            return Err("The Witcher 3 game directory selection aborted.".to_string());
        }
    }
    Ok(picked_path)
}
// ----------------------------------------------------------------------------
