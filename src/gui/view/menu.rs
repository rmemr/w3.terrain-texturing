// ----------------------------------------------------------------------------
#[inline]
pub fn show(
    egui_ctx: &mut EguiContext,
    ui_state: &UiState,
    gui_event: &mut EventWriter<GuiAction>,
    project: &project::ProjectSettings,
    #[cfg(feature = "witcher3")] w3conf: &witcher3::Witcher3Config,
) {
    egui::TopBottomPanel::top("top_panel").show(egui_ctx.ctx_mut(), |ui| {
        egui::menu::bar(ui, |ui| {
            ui.set_enabled(ui_state.enabled && !ui_state.project_is_loading);

            ui.menu_button("Project", |ui| {
                // -- no project loaded
                ui.add_enabled_ui(!ui_state.project_open, |ui| {
                    if ui.button("Open").clicked() {
                        ui.close_menu();

                        if let Some(file) = rfd::FileDialog::new()
                            .add_filter("world definition", &["yml"])
                            .pick_file()
                        {
                            gui_event.send(GuiAction::OpenProject(file))
                        }
                    }
                });

                // -- opened project
                ui.add_enabled_ui(ui_state.project_open, |ui| {
                    // quicksave in same dir only if allowed (== no example project)
                    // and some data modified
                    let quicksaving_active = project.is_modified() && !project.no_quicksave();
                    if ui
                        .add_enabled(quicksaving_active, egui::Button::new("Save"))
                        .clicked()
                    {
                        ui.close_menu();
                        gui_event.send(GuiAction::QuickSaveProject);
                    }
                    if ui.button("Save in...").clicked() {
                        ui.close_menu();
                        if let Some(directory) = rfd::FileDialog::new().pick_folder() {
                            gui_event.send(GuiAction::SaveProjectIn(directory));
                        }
                    }

                    ui.separator();
                    if ui.button("Close").clicked() {
                        ui.close_menu();
                        gui_event.send(GuiAction::CloseProject);
                    }
                });

                ui.separator();
                if ui.button("Quit editor").clicked() {
                    ui.close_menu();
                    gui_event.send(GuiAction::QuitRequest);
                }
            });

            #[cfg(feature = "witcher3")]
            super::w3plugin::show_menu(ui, ui_state, w3conf, gui_event);

            #[cfg(debug_assertions)]
            {
                ui.add_space(50.0);
                ui.separator();
                crate::gui::debug::show_menu(ui, ui_state, gui_event);
            }
        });
    });
}
// ----------------------------------------------------------------------------
use bevy::prelude::EventWriter;
use bevy_egui::{egui, EguiContext};

use crate::project;

use super::{GuiAction, UiState};
// ----------------------------------------------------------------------------
