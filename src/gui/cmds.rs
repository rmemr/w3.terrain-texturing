//
// gui::cmds - more complex actions with (possible) side effects
//
// ----------------------------------------------------------------------------
pub(super) fn open_project(
    path: &Path,
    current_project: &mut ProjectSettings,
    env_registry: &EnvironmentConfigRegistry,
    materialset_registry: &MaterialSetRegistry,
) -> Result<GuiAction, String> {
    // try to load and parse definition
    *current_project = project::load_def(path, env_registry, materialset_registry)?;

    // before opening dialog verify all loaded definitions and peek into given
    // maps to check for proper size and image types
    current_project.validate("project settings")?;

    // show dialog with loaded project settings to confirm loading/generating terrain
    Ok(GuiAction::OpenModalDialog(
        ModalDialog::LoadedProjectSettings,
    ))
}
// ----------------------------------------------------------------------------
pub(super) fn close_project(app_state: &mut State<EditorState>) {
    app_state.overwrite_set(EditorState::NoTerrainData).ok();
}
// ----------------------------------------------------------------------------
pub(super) fn start_quick_saving_project(
    app_state: &mut State<EditorState>,
    project: &mut ProjectSettings,
    full_backup: bool,
) -> Result<Option<GuiAction>, String> {

    if project.is_modified() && !project.no_quicksave() {
        // backup only the changeable data
        project::backup_project(project.project_directory(), project, full_backup)?;

        app_state.overwrite_set(EditorState::ProjectSaving).ok();
    }
    Ok(None)
}
// ----------------------------------------------------------------------------
pub(super) fn start_saving_project_in(
    app_state: &mut State<EditorState>,
    project: &mut ProjectSettings,
    directory: &Path,
) -> Result<Option<GuiAction>, String> {
    // normalize dirs for comparison
    let current_dir = project.project_directory().canonicalize();
    let new_dir = directory.canonicalize();

    // perform save only in *new* directory
    let is_new_directory = match (current_dir, new_dir) {
        (Err(_), Ok(_)) => true,
        (Ok(current_dir), Ok(new_dir)) => current_dir != new_dir,
        _ => false,
    };

    if is_new_directory {
        // set new directory
        project.set_project_directory(directory);

        // mark fully changed so everything will be saved
        project.set_all_modified();

        app_state.overwrite_set(EditorState::ProjectSaving).ok();
    }
    Ok(None)
}
// ----------------------------------------------------------------------------
use std::path::Path;

use bevy::prelude::*;

use crate::environment::EnvironmentConfigRegistry;
use crate::project;
use crate::project::{MaterialSetRegistry, ProjectSettings};

use super::{EditorState, GuiAction, ModalDialog};
// ----------------------------------------------------------------------------
