//
// gui::update - simple(r) actions for updating state, mapping to other actions
//
// ----------------------------------------------------------------------------
use bevy::prelude::*;

use crate::atmosphere::AtmosphereMat;
use crate::cmds;
use crate::config;
use crate::project;

use crate::environment::{DayNightCycle, SunPositionSettings};
use crate::terrain_tiles::TerrainMeshSettings;
use crate::EditorState;

use super::ModalDialog;
use super::UiState;
use super::{AtmosphereSetting, DayNightCycleSetting, MeshSetting, RenderSetting, SunSetting};
// ----------------------------------------------------------------------------
pub(super) fn update_daynight_cycle_settings(
    action: &DayNightCycleSetting,
    daynight_cycle: &mut ResMut<DayNightCycle>,
) {
    use DayNightCycleSetting::*;
    match action {
        SetTimeOfDay(v) => {
            daynight_cycle.update_time_of_day(*v);
            daynight_cycle.set_cycle_speed(0);
            daynight_cycle.activate_cycle(false);
        }
        SetCycleSpeed(v) => {
            daynight_cycle.set_cycle_speed(*v);
            daynight_cycle.activate_cycle(*v > 0);
        }
    }
}
// ----------------------------------------------------------------------------
pub(super) fn update_sun_settings(
    action: &SunSetting,
    sun: &mut Option<ResMut<SunPositionSettings>>,
) {
    use SunSetting::*;

    if let Some(sun) = sun {
        match action {
            SetPlaneTilt(v) => sun.set_plane_tilt(*v),
            SetPlaneYaw(v) => sun.set_plane_yaw(*v),
            SetPlaneHeight(v) => sun.set_plane_height(*v),
            ToggleDebugMesh => sun.toggle_debug_mesh(),
        }
    }
}
// ----------------------------------------------------------------------------
#[rustfmt::skip]
pub(super) fn update_atmosphere_settings(
    action: &AtmosphereSetting,
    atmosphere: &mut Option<ResMut<AtmosphereMat>>,
) {
    use AtmosphereSetting::*;

    if let Some(atmosphere) = atmosphere {
        match action {
            SetRayOrigin(ray_origin) => atmosphere.set_ray_origin(*ray_origin),
            SetSunIntensity(sun_intensity) => atmosphere.set_sun_intensity(*sun_intensity),
            SetPlanetRadius(planet_radius) => atmosphere.set_planet_radius(*planet_radius),
            SetAtmosphereRadius(atmosphere_radius) => atmosphere.set_atmosphere_radius(*atmosphere_radius),
            SetRayleighScattering(coefficient) => atmosphere.set_rayleigh_scattering_coefficient(*coefficient),
            SetRayleighScaleHeight(scale) => atmosphere.set_rayleigh_scale_height(*scale),
            SetMieScattering(coefficient) => atmosphere.set_mie_scattering_coefficient(*coefficient),
            SetMieScaleHeight(scale) => atmosphere.set_mie_scale_height(*scale),
            SetMieScatteringDirection(direction) => atmosphere.set_mie_scattering_direction(*direction),
            ResetToDefault => **atmosphere = AtmosphereMat::default(),
        }
    }
}
// ----------------------------------------------------------------------------
#[rustfmt::skip]
pub(super) fn update_mesh_settings(
    action: &MeshSetting,
    mesh_settings: &mut Option<ResMut<TerrainMeshSettings>>,
) {
    if let Some(mesh) = mesh_settings {
        match action {
            MeshSetting::SetLodCount(count) => mesh.set_lodcount(*count),
            MeshSetting::SetLodMinError(error) => mesh.set_min_error(*error),
            MeshSetting::SetLodMaxError(error) => mesh.set_max_error(*error),
            MeshSetting::SetLodMaxDistance(distance) => mesh.set_max_distance(*distance),
            MeshSetting::SetLodError(slot, error) => mesh.set_lod_error(*slot, *error),
            MeshSetting::SetLodDistance(slot, distance) => mesh.set_lod_distance(*slot, *distance),
            MeshSetting::FreezeLods => mesh.ignore_anchor = !mesh.ignore_anchor,
            MeshSetting::ResetToDefault => **mesh = TerrainMeshSettings::default(),
        }
    }
}
// ----------------------------------------------------------------------------
pub(super) fn update_render_settings(
    action: &RenderSetting,
    task_manager: &mut cmds::AsyncCommandManager,
) {
    match action {
        RenderSetting::OverlayWireframe(_) => {
            task_manager.add_new(cmds::GenerateTerrainMeshes.into());
        }
    }
}
// ----------------------------------------------------------------------------
pub(super) fn start_terrain_loading(
    app_state: &mut State<EditorState>,
    project: &mut project::ProjectSettings,
    terrain_config: &mut config::TerrainConfig,
    new_project: &project::ProjectSettings,
) {
    *project = (*new_project).clone();
    *terrain_config = new_project.terrain_config().clone();

    app_state.overwrite_set(EditorState::TerrainLoading).ok();
}
// ----------------------------------------------------------------------------
// modal dialog
// ----------------------------------------------------------------------------
pub(super) fn open_modal_dialog(
    app_state: &mut State<EditorState>,
    ui_state: &mut UiState,
    dialog: ModalDialog,
) {
    ui_state.update(EditorState::ModalDialog);
    ui_state.modal.open(dialog);
    app_state.overwrite_push(EditorState::ModalDialog).ok();
}
// ----------------------------------------------------------------------------
pub(super) fn open_modal_dialog_with_exit_state(
    app_state: &mut State<EditorState>,
    ui_state: &mut UiState,
    new_state: EditorState,
    dialog: ModalDialog,
) {
    ui_state.update(EditorState::ModalDialog);
    ui_state.modal.open(dialog);
    ui_state.modal.set_exit_state(new_state);
    // Note: do not push new state. instead overwrite current
    app_state.overwrite_set(EditorState::ModalDialog).ok();
}
// ----------------------------------------------------------------------------
pub(super) fn close_modal_dialog(app_state: &mut State<EditorState>, ui_state: &mut UiState) {
    if app_state.current() == &EditorState::ModalDialog && ui_state.modal.close() {
        // if all modal dialogs are closed return to previous
        // editor state OR set exit state
        if let Some(exit_state) = ui_state.modal.exit_state.take() {
            app_state.overwrite_set(exit_state).ok();
        } else {
            app_state.overwrite_pop().ok();
        }
    }
}
// ----------------------------------------------------------------------------
