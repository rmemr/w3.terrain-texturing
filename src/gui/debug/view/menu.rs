// ----------------------------------------------------------------------------
#[rustfmt::skip]
pub fn show_menu(
    ui: &mut egui::Ui,
    ui_state: &UiState,
    gui_event: &mut EventWriter<GuiAction>,
) {
    use crate::project::ProjectSettings;
    use GuiAction::LoadTerrain;

    ui.set_enabled(!ui_state.project_is_loading);

    ui.menu_button("Debug", |ui| {
        let mut result = None;
        if ui.add_enabled(ui_state.project_open && !ui_state.debug.show_clipmaps, Button::new("show clipmaps")).clicked() {
            result = Some(GuiAction::DebugShowClipmap(true));
        }
        ui.separator();

        ui.set_enabled(!ui_state.project_open);
        ui.label("Test Terrain");
        if ui.button("Load Test terrain (1024)").clicked() { result = Some(LoadTerrain(Box::new(ProjectSettings::test_terrain(1024)))); }
        if ui.button("Load Test terrain (2048)").clicked() { result = Some(LoadTerrain(Box::new(ProjectSettings::test_terrain(2048)))); }
        if ui.button("Load Test terrain (4096)").clicked() { result = Some(LoadTerrain(Box::new(ProjectSettings::test_terrain(4096)))); }
        ui.separator();

        if let Some(event) = result {
            ui.close_menu();
            gui_event.send(event);
        }
    });
}
// ----------------------------------------------------------------------------
use bevy::prelude::*;
use bevy_egui::egui::{self, Button};

use super::{GuiAction, UiState};
// ----------------------------------------------------------------------------
