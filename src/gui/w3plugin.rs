// ----------------------------------------------------------------------------
use std::path::PathBuf;

use bevy::prelude::*;

use crate::config;
use crate::project;
use crate::EditorState;

use witcher3::{ExtractionRequest, Witcher3HubId};
// ----------------------------------------------------------------------------
#[derive(Debug, Clone)]
pub enum GuiAction {
    SetGamePath(PathBuf),
    LoadHub(Witcher3HubId),
    ExtractTerrainData(Vec<Witcher3HubId>),
    ExtractTerrainTextures(Vec<Witcher3HubId>),
}
// ----------------------------------------------------------------------------
pub(super) fn handle_witcher3_plugin_action(
    w3conf: &mut witcher3::Witcher3Config,
    action: &GuiAction,
    commands: &mut Commands,
    app_state: &mut State<EditorState>,
    project: &mut project::ProjectSettings,
    terrain_config: &mut config::TerrainConfig,
) {
    match action {
        GuiAction::SetGamePath(path) => w3conf.set_gamedir(path),
        GuiAction::LoadHub(hubid) => {
            let w3conf = w3conf.terrain();
            *project = (w3conf.terrain_dir(), w3conf.texture_dir(), hubid.settings()).into();
            *terrain_config = project.terrain_config().clone();

            app_state.overwrite_set(EditorState::TerrainLoading).ok();
        }
        GuiAction::ExtractTerrainData(hubs) => {
            let requests = hubs
                .iter()
                .copied()
                .map(ExtractionRequest::TerrainData)
                .collect::<Vec<_>>();

            commands.insert_resource(requests);
            app_state
                .overwrite_set(EditorState::ExtractWitcher3Data)
                .ok();
        }
        GuiAction::ExtractTerrainTextures(hubs) => {
            let requests = hubs
                .iter()
                .copied()
                .map(ExtractionRequest::TerrainTextures)
                .collect::<Vec<_>>();

            commands.insert_resource(requests);
            app_state
                .overwrite_set(EditorState::ExtractWitcher3Data)
                .ok();
        }
    }
}
// ----------------------------------------------------------------------------
