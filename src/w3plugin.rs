// ----------------------------------------------------------------------------
use bevy::prelude::*;

use super::{EditorEvent, EditorState};
use crate::cmds;

use witcher3::ExtractionRequest;
// ----------------------------------------------------------------------------
impl<'a> From<&'a witcher3::ProgressTrackingState> for super::cmds::TrackedProgress {
    fn from(state: &'a witcher3::ProgressTrackingState) -> Self {
        match state.request {
            ExtractionRequest::TerrainData(hubid) => {
                crate::cmds::TrackedProgress::W3PluginProgress(TrackedProgress::ExtractTerrainData(
                    hubid,
                    state.current,
                    state.max,
                    state.task_progress_msg(),
                ))
            }
            ExtractionRequest::TerrainTextures(hubid) => {
                crate::cmds::TrackedProgress::W3PluginProgress(
                    TrackedProgress::ExtractTerrainTextures(
                        hubid,
                        state.current,
                        state.max,
                        state.task_progress_msg(),
                    ),
                )
            }
            ExtractionRequest::None => unreachable!(),
        }
    }
}
// ----------------------------------------------------------------------------
#[derive(Clone, Eq, Debug)]
pub enum TrackedProgress {
    ExtractTerrainData(witcher3::Witcher3HubId, usize, usize, Option<String>),
    ExtractTerrainTextures(witcher3::Witcher3HubId, usize, usize, Option<String>),
}
// ----------------------------------------------------------------------------
impl std::hash::Hash for TrackedProgress {
    // ------------------------------------------------------------------------
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        match self {
            TrackedProgress::ExtractTerrainData(hubid, _, _, _) => hubid.hash(state),
            TrackedProgress::ExtractTerrainTextures(hubid, _, _, _) => hubid.hash(state),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl std::cmp::PartialEq for TrackedProgress {
    // ------------------------------------------------------------------------
    fn eq(&self, other: &Self) -> bool {
        match self {
            Self::ExtractTerrainData(hubid, _, _, _) => match other {
                TrackedProgress::ExtractTerrainData(other, _, _, _) => hubid.eq(other),
                _ => false,
            },
            Self::ExtractTerrainTextures(hubid, _, _, _) => match other {
                TrackedProgress::ExtractTerrainTextures(other, _, _, _) => hubid.eq(other),
                _ => false,
            },
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl cmds::TrackedProgressApi for TrackedProgress {
    // ------------------------------------------------------------------------
    fn is_finished(&self) -> bool {
        match self {
            TrackedProgress::ExtractTerrainData(_, current, max, _) => current >= max,
            TrackedProgress::ExtractTerrainTextures(_, current, max, _) => current >= max,
        }
    }
    // ------------------------------------------------------------------------
    fn progress(&self) -> f32 {
        match self {
            TrackedProgress::ExtractTerrainData(_, current, max, _) => {
                *current as f32 / *max as f32
            }
            TrackedProgress::ExtractTerrainTextures(_, current, max, _) => {
                *current as f32 / *max as f32
            }
        }
    }
    // ------------------------------------------------------------------------
    fn progress_msg(&self) -> String {
        match self {
            TrackedProgress::ExtractTerrainData(_, _, _, msg) => msg.as_ref().unwrap().to_string(),
            TrackedProgress::ExtractTerrainTextures(_, _, _, msg) => {
                msg.as_ref().unwrap().to_string()
            }
        }
    }
    // ------------------------------------------------------------------------
    fn finished_msg(&self) -> &str {
        "finished."
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// systems
// ----------------------------------------------------------------------------
#[cfg(feature = "witcher3")]
pub(super) fn watch_data_extraction(
    mut app_state: ResMut<State<EditorState>>,
    mut extraction_events: EventReader<witcher3::ExtractionProgress>,
    mut editor_events: EventWriter<EditorEvent>,
    mut w3conf: ResMut<witcher3::Witcher3Config>,
) {
    use witcher3::ExtractionProgress::*;

    for event in extraction_events.iter() {
        match event {
            ProgressTrackingStart(caption, tasks) => {
                info!("starting witcher 3 data extraction...");
                editor_events.send(EditorEvent::ProgressTrackingStart(
                    caption.into(),
                    tasks.iter().map(|task| task.into()).collect(),
                ));
            }
            ProgressTrackingUpdate(progress) => {
                editor_events.send(EditorEvent::ProgressTrackingUpdate(progress.into()));
            }
            ProgressTrackingCancel => {
                editor_events.send(EditorEvent::ProgressTrackingCancel);
                app_state.overwrite_set(EditorState::NoTerrainData).ok();
            }
            ExtractionFinished => {
                info!("extraction finished");
                // rescan for new hubs
                w3conf.check_available_hub_data();
                app_state.overwrite_set(EditorState::NoTerrainData).ok();
            }
        }
    }
}
// ----------------------------------------------------------------------------
