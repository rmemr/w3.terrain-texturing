// ----------------------------------------------------------------------------
use bevy::prelude::*;
use bevy::utils::HashMap;

use super::{ColorCurveEntry, ScalarCurveEntry};
// ----------------------------------------------------------------------------
#[derive(Default)]
pub struct EnvironmentConfigRegistry {
    pub(super) envs: HashMap<String, EnvironmentConfig>,
}
// ----------------------------------------------------------------------------
#[derive(Default, Clone)]
pub(super) struct EnvironmentConfig {
    pub sun: SunConfig,
    pub fog: FogConfig,
}
// ----------------------------------------------------------------------------
#[derive(Clone)]
pub(super) struct SunConfig {
    pub color: Vec<ColorCurveEntry>,
}
// ----------------------------------------------------------------------------
#[derive(Clone)]
pub(super) struct FogConfig {
    pub appear_distance: Vec<ScalarCurveEntry>,
    pub appear_range: Vec<ScalarCurveEntry>,
    pub color_front: Vec<ColorCurveEntry>,
    pub color_middle: Vec<ColorCurveEntry>,
    pub color_back: Vec<ColorCurveEntry>,
    pub density: Vec<ScalarCurveEntry>,
    pub final_exp: Vec<ScalarCurveEntry>,
    pub distance_clamp: Vec<ScalarCurveEntry>,
    pub vertical_offset: Vec<ScalarCurveEntry>,
    pub vertical_density: Vec<ScalarCurveEntry>,
    pub vertical_density_light_front: Vec<ScalarCurveEntry>,
    pub vertical_density_light_back: Vec<ScalarCurveEntry>,
    // sky_denity_scale: Vec<ScalarCurveEntry>,
    // clouds_density_scale: Vec<ScalarCurveEntry>,
    // sky_vertical_density_light_front_scale: Vec<ScalarCurveEntry>,
    // sky_vertical_density_light_back_scale: Vec<ScalarCurveEntry>,
    pub vertical_density_rim_range: Vec<ScalarCurveEntry>,
    pub custom_color: Vec<ColorCurveEntry>,
    pub custom_color_start: Vec<ScalarCurveEntry>,
    pub custom_color_range: Vec<ScalarCurveEntry>,
    pub custom_amount_scale: Vec<ScalarCurveEntry>,
    pub custom_amount_scale_start: Vec<ScalarCurveEntry>,
    pub custom_amount_scale_range: Vec<ScalarCurveEntry>,
    pub aerial_color_front: Vec<ColorCurveEntry>,
    pub aerial_color_middle: Vec<ColorCurveEntry>,
    pub aerial_color_back: Vec<ColorCurveEntry>,
    pub aerial_final_exp: Vec<ScalarCurveEntry>,
}
// ----------------------------------------------------------------------------
// systen
// ----------------------------------------------------------------------------
pub(super) fn setup_env_registry(mut env_registry: ResMut<EnvironmentConfigRegistry>) {
    // TODO scan for available envs?
    env_registry.envs.insert("template".into(), template_env_settings().unwrap());
}
// ----------------------------------------------------------------------------
impl EnvironmentConfigRegistry {
    // ------------------------------------------------------------------------
    pub fn contains(&self, path: &str) -> bool {
        self.envs.contains_key(path)
    }
    // ------------------------------------------------------------------------
    pub(super) fn get(&self, path: &str) -> Result<EnvironmentConfig, String> {
        self.envs.get(path).cloned().ok_or_else(|| {
            format!(
                "error loading enviroment settings: path [{}] not found.",
                path
            )
        })
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// defaults
// ----------------------------------------------------------------------------
impl Default for SunConfig {
    // ------------------------------------------------------------------------
    fn default() -> Self {
        Self {
            color: vec![ColorCurveEntry::try_from(("00:00", 255.0, 255.0, 255.0, 100.0)).unwrap()],
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl Default for FogConfig {
    // ------------------------------------------------------------------------
    fn default() -> Self {
        let color = ColorCurveEntry::try_from(("00:00", 255.0, 255.0, 255.0, 100.0)).unwrap();
        let scalar_small = ScalarCurveEntry::try_from(("00:00", 0.0)).unwrap();
        let scalar_big = ScalarCurveEntry::try_from(("00:00", 32768.0)).unwrap();
        Self {
            appear_distance: vec![scalar_small.clone()],
            appear_range: vec![scalar_big.clone()],
            color_front: vec![color.clone()],
            color_middle: vec![color.clone()],
            color_back: vec![color.clone()],
            density: vec![ScalarCurveEntry::try_from(("17:00", 0.001)).unwrap()],
            final_exp: vec![scalar_small.clone()],
            distance_clamp: vec![scalar_big.clone()],
            vertical_offset: vec![scalar_big.clone()],
            // vertical_density: vec![scalar_small.clone()],
            vertical_density: vec![ScalarCurveEntry::try_from(("00:00", -0.0232)).unwrap()],
            vertical_density_light_front: vec![scalar_small.clone()],
            vertical_density_light_back: vec![scalar_small.clone()],
            vertical_density_rim_range: vec![scalar_small.clone()],
            custom_color: vec![color.clone()],
            custom_color_start: vec![scalar_big.clone()],
            custom_color_range: vec![scalar_big],
            custom_amount_scale: vec![scalar_small.clone()],
            custom_amount_scale_start: vec![scalar_small.clone()],
            custom_amount_scale_range: vec![scalar_small.clone()],
            aerial_color_front: vec![color.clone()],
            aerial_color_middle: vec![color.clone()],
            aerial_color_back: vec![color],
            aerial_final_exp: vec![scalar_small],
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// hardcoded test values (based on prolog env)
// ----------------------------------------------------------------------------
#[rustfmt::skip]
#[allow(clippy::excessive_precision, clippy::approx_constant)]
fn template_env_settings() -> Result<EnvironmentConfig, String> {
    Ok(EnvironmentConfig {
        sun: SunConfig {
            color: vec![
                ColorCurveEntry::try_from(("00:00", 61.0, 113.0, 155.0, 85.08))?,
                ColorCurveEntry::try_from(("03:30", 15.0, 11.0, 4.0, 85.08))?,
                ColorCurveEntry::try_from(("04:30", 252.0, 196.0, 123.0, 85.08))?,
                ColorCurveEntry::try_from(("10:00", 196.0, 175.0, 152.0, 85.08))?,
                ColorCurveEntry::try_from(("14:30", 194.0, 173.0, 150.0, 85.08))?,
                ColorCurveEntry::try_from(("20:00", 255.0, 145.0, 11.0, 85.08))?,
                ColorCurveEntry::try_from(("21:15", 31.363, 28.431, 26.726, 85.08))?,
                ColorCurveEntry::try_from(("21:20", 7.0, 7.0, 7.0, 85.08))?,
                ColorCurveEntry::try_from(("23:59", 61.0, 113.0, 155.0, 85.08))?,
            ]
        },
        fog: FogConfig {
            appear_distance:    vec![ScalarCurveEntry::try_from(("00:00", 1.0))?],
            appear_range:       vec![ScalarCurveEntry::try_from(("00:00", 66.0657348633))?],
            color_front: vec![
                ColorCurveEntry::try_from(("00:00", 37.012, 64.993, 75.993, 10.378))?,
                ColorCurveEntry::try_from(("03:30", 62.0, 36.0, 20.0, 10.359))?,
                ColorCurveEntry::try_from(("05:00", 145.0, 96.0, 25.0, 10.357))?,
                ColorCurveEntry::try_from(("06:30", 167.0, 158.0, 120.0, 10.441))?,
                ColorCurveEntry::try_from(("15:30", 162.0, 189.0, 213.0, 10.420))?,
                ColorCurveEntry::try_from(("19:00", 118.0, 74.0, 48.0, 10.452))?,
                ColorCurveEntry::try_from(("20:00", 126.0, 80.0, 52.0, 10.452))?,
                ColorCurveEntry::try_from(("20:30", 94.0, 65.0, 47.0, 10.452))?,
            ],
            color_middle: vec![
                ColorCurveEntry::try_from(("00:00", 30.0, 53.0, 62.0, 8.999))?,
                ColorCurveEntry::try_from(("03:30", 20.335, 55.741, 66.741, 7.945))?,
                ColorCurveEntry::try_from(("05:00", 85.0, 104.0, 118.0, 9.016))?,
                ColorCurveEntry::try_from(("06:30", 100.0, 132.0, 155.0, 9.018))?,
                ColorCurveEntry::try_from(("15:30", 135.0, 174.0, 209.0, 9.014))?,
                ColorCurveEntry::try_from(("19:00", 67.0, 86.0, 95.0, 9.021))?,
                ColorCurveEntry::try_from(("20:00", 58.0, 69.0, 74.0, 8.016))?,
            ],
            color_back: vec![
                ColorCurveEntry::try_from(("00:00", 30.0, 53.0, 62.0, 7.966))?,
                ColorCurveEntry::try_from(("03:30", 19.0, 55.0, 66.0, 7.944))?,
                ColorCurveEntry::try_from(("05:00", 70.0, 89.0, 104.0, 9.016))?,
                ColorCurveEntry::try_from(("06:30", 119.0, 154.0, 184.0, 8.471))?,
                ColorCurveEntry::try_from(("16:30", 119.0, 154.0, 184.0, 8.471))?,
                ColorCurveEntry::try_from(("19:30", 55.0, 72.0, 81.0, 7.925))?,
                ColorCurveEntry::try_from(("21:00", 55.0, 72.0, 81.0, 7.925))?,
            ],
            density: vec![
                ScalarCurveEntry::try_from(("17:00", 0.001))?,
                ScalarCurveEntry::try_from(("19:30", 0.002))?,
            ],
            final_exp: vec![
                ScalarCurveEntry::try_from(("02:00", 0.449162364))?,
                ScalarCurveEntry::try_from(("03:30", 0.7669311166))?,
                ScalarCurveEntry::try_from(("04:00", 0.8201212287))?,
                ScalarCurveEntry::try_from(("04:30", 0.8413972259))?,
                ScalarCurveEntry::try_from(("07:30", 1.2095916271))?,
                ScalarCurveEntry::try_from(("12:00", 0.9761633873))?,
                ScalarCurveEntry::try_from(("17:00", 0.9761630297))?,
                ScalarCurveEntry::try_from(("21:00", 0.7475311756))?,
                ScalarCurveEntry::try_from(("22:00", 0.5865037441))?,
            ],
            distance_clamp: vec![
                ScalarCurveEntry::try_from(("00:00", 16707.900390625))?,
            ],
            vertical_offset: vec![
                ScalarCurveEntry::try_from(("00:00", 39.4300003052))?,
            ],
            vertical_density: vec![
                ScalarCurveEntry::try_from(("00:00", -0.0232305992))?,
            ],
            vertical_density_light_front: vec![
                ScalarCurveEntry::try_from(("00:00", 0.9947260022))?,
            ],
            vertical_density_light_back: vec![
                ScalarCurveEntry::try_from(("00:00", 1.0))?,
            ],
            vertical_density_rim_range: vec![
                ScalarCurveEntry::try_from(("00:00", 1.0))?,
            ],
            custom_color: vec![
                ColorCurveEntry::try_from(("00:00", 3.142, 19.264, 31.347, 1.987))?,
                ColorCurveEntry::try_from(("03:30", 26.008, 38.008, 50.119, 9.192))?,
                ColorCurveEntry::try_from(("04:00", 77.0, 59.0, 14.0, 9.187))?,
                ColorCurveEntry::try_from(("05:00", 149.0, 114.0, 25.0, 9.187))?,
                ColorCurveEntry::try_from(("07:30", 218.713, 213.212, 187.414, 5.371))?,
                ColorCurveEntry::try_from(("11:30", 246.272, 243.531, 231.399, 5.179))?,
                ColorCurveEntry::try_from(("18:00", 205.904, 206.281, 198.785, 2.72))?,
                ColorCurveEntry::try_from(("20:00", 253.0, 58.0, 1.0, 2.082))?,
                ColorCurveEntry::try_from(("20:15", 57.0, 59.0, 61.0, 2.047))?,
            ],
            custom_color_start: vec![
                ScalarCurveEntry::try_from(("02:00", -1.0688883066))?,
                ScalarCurveEntry::try_from(("03:30", 0.2639680505))?,
                ScalarCurveEntry::try_from(("05:00", 1.0108048916))?,
                ScalarCurveEntry::try_from(("05:30", 0.3837502003))?,
                ScalarCurveEntry::try_from(("17:30", 0.3083299398))?,
                ScalarCurveEntry::try_from(("19:00", 1.5800062418))?,
                ScalarCurveEntry::try_from(("20:00", 1.6010992527))?,
                ScalarCurveEntry::try_from(("20:20", -0.4585551023))?,
                ScalarCurveEntry::try_from(("20:30", -1.0515730381))?,
            ],
            custom_color_range: vec![
                ScalarCurveEntry::try_from(("01:00", 3.6806063652))?,
                ScalarCurveEntry::try_from(("04:30", 3.8118515015))?,
                ScalarCurveEntry::try_from(("07:30", 1.9127099514))?,
                ScalarCurveEntry::try_from(("16:00", 1.9300076962))?,
                ScalarCurveEntry::try_from(("19:30", 0.650424242))?,
                ScalarCurveEntry::try_from(("20:30", 2.8878257275))?,
                ScalarCurveEntry::try_from(("21:00", 3.3950767517))?,
            ],
            custom_amount_scale: vec![
                ScalarCurveEntry::try_from(("00:00", 1.0))?,
            ],
            custom_amount_scale_start: vec![
                ScalarCurveEntry::try_from(("00:00", 1.0))?,
            ],
            custom_amount_scale_range: vec![
                ScalarCurveEntry::try_from(("00:00", 1.0))?,
            ],
            aerial_color_front: vec![
                ColorCurveEntry::try_from(("00:00", 0.467, 19.104, 32.557, 12.123))?,
            ],
            aerial_color_middle: vec![
                ColorCurveEntry::try_from(("00:00", 17.363, 16.916, 31.84, 10.044))?,
            ],
            aerial_color_back: vec![
                ColorCurveEntry::try_from(("00:00", 7.0383, 13.51, 26.941, 10.013))?,
            ],
            aerial_final_exp: vec![
                ScalarCurveEntry::try_from(("02:30", 0.2983746529))?,
                ScalarCurveEntry::try_from(("03:30", 0.7329537868))?,
                ScalarCurveEntry::try_from(("04:00", 0.7536482811))?,
                ScalarCurveEntry::try_from(("12:30", 40.0425224304))?,
                ScalarCurveEntry::try_from(("19:30", 1.4706077576))?,
                ScalarCurveEntry::try_from(("21:30", 0.3510617614))?,
                ScalarCurveEntry::try_from(("23:00", 0.2983746529))?,
            ],
        }
    })
}
// ----------------------------------------------------------------------------
