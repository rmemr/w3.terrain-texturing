// ----------------------------------------------------------------------------
use bevy::prelude::*;

use super::definition::{EnvironmentConfig, FogConfig, SunConfig};
use super::utils::{ColorCurveEntry, ScalarCurveEntry};
use super::EnvironmentConfigRegistry;

use witcher3::environment::{
    ColorEntry, EnvDefinition, EnvFogSettings, EnvSettings, EnvSunSettings, ScalarEntry,
};
// ----------------------------------------------------------------------------
// systen
// ----------------------------------------------------------------------------
pub(super) fn setup_env_registry(mut env_registry: ResMut<EnvironmentConfigRegistry>) {
    use EnvDefinition::*;

    for env_id in [
        PrologColorsSunset,
        WinterEpilog,
        SkelligeBrown,
        NovigradSunset,
        KaerMorhenV09,
        Wyzima,
        IsleOfMistDark,
        SpiralDarkValley,
        SpiralDesertClear,
        SpiralElvenCity,
        SpiralSnowHour,
        SunnyBobV7,
    ] {
        match env_id.settings().try_into() {
            Ok(env) => {
                env_registry.envs.insert(env_id.path().into(), env);
            }
            Err(msg) => {
                error!(
                    "failed to convert environment definition {:?}: {}",
                    env_id, msg
                );
            }
        }
    }
}
// ----------------------------------------------------------------------------
impl TryFrom<EnvSettings> for EnvironmentConfig {
    type Error = String;

    fn try_from(env: EnvSettings) -> Result<Self, Self::Error> {
        Ok(EnvironmentConfig {
            sun: env.sun.try_into()?,
            fog: env.fog.try_into()?,
        })
    }
}
// ----------------------------------------------------------------------------
impl TryFrom<EnvSunSettings> for SunConfig {
    type Error = String;

    fn try_from(settings: EnvSunSettings) -> Result<Self, Self::Error> {
        Ok(SunConfig {
            color: settings
                .color
                .iter()
                .map(ColorCurveEntry::try_from)
                .collect::<Result<Vec<_>, _>>()?,
        })
    }
}
// ----------------------------------------------------------------------------
impl TryFrom<EnvFogSettings> for FogConfig {
    type Error = String;

    #[rustfmt::skip]
    fn try_from(settings: EnvFogSettings) -> Result<Self, Self::Error> {
        Ok(FogConfig {
            appear_distance: settings.appear_distance.iter().map(ScalarCurveEntry::try_from).collect::<Result<Vec<_>, _>>()?,
            appear_range: settings.appear_range.iter().map(ScalarCurveEntry::try_from).collect::<Result<Vec<_>, _>>()?,
            color_front: settings.color_front.iter().map(ColorCurveEntry::try_from).collect::<Result<Vec<_>, _>>()?,
            color_middle: settings.color_middle.iter().map(ColorCurveEntry::try_from).collect::<Result<Vec<_>, _>>()?,
            color_back: settings.color_back.iter().map(ColorCurveEntry::try_from).collect::<Result<Vec<_>, _>>()?,
            density: settings.density.iter().map(ScalarCurveEntry::try_from).collect::<Result<Vec<_>, _>>()?,
            final_exp: settings.final_exp.iter().map(ScalarCurveEntry::try_from).collect::<Result<Vec<_>, _>>()?,
            distance_clamp: settings.distance_clamp.iter().map(ScalarCurveEntry::try_from).collect::<Result<Vec<_>, _>>()?,
            vertical_offset: settings.vertical_offset.iter().map(ScalarCurveEntry::try_from).collect::<Result<Vec<_>, _>>()?,
            vertical_density: settings.vertical_density.iter().map(ScalarCurveEntry::try_from).collect::<Result<Vec<_>, _>>()?,
            vertical_density_light_front: settings.vertical_density_light_front.iter().map(ScalarCurveEntry::try_from).collect::<Result<Vec<_>, _>>()?,
            vertical_density_light_back: settings.vertical_density_light_back.iter().map(ScalarCurveEntry::try_from).collect::<Result<Vec<_>, _>>()?,
            vertical_density_rim_range: settings.vertical_density_rim_range.iter().map(ScalarCurveEntry::try_from).collect::<Result<Vec<_>, _>>()?,
            custom_color: settings.custom_color.iter().map(ColorCurveEntry::try_from).collect::<Result<Vec<_>, _>>()?,
            custom_color_start: settings.custom_color_start.iter().map(ScalarCurveEntry::try_from).collect::<Result<Vec<_>, _>>()?,
            custom_color_range: settings.custom_color_range.iter().map(ScalarCurveEntry::try_from).collect::<Result<Vec<_>, _>>()?,
            custom_amount_scale: settings.custom_amount_scale.iter().map(ScalarCurveEntry::try_from).collect::<Result<Vec<_>, _>>()?,
            custom_amount_scale_start: settings.custom_amount_scale_start.iter().map(ScalarCurveEntry::try_from).collect::<Result<Vec<_>, _>>()?,
            custom_amount_scale_range: settings.custom_amount_scale_range.iter().map(ScalarCurveEntry::try_from).collect::<Result<Vec<_>, _>>()?,
            aerial_color_front: settings.aerial_color_front.iter().map(ColorCurveEntry::try_from).collect::<Result<Vec<_>, _>>()?,
            aerial_color_middle: settings.aerial_color_middle.iter().map(ColorCurveEntry::try_from).collect::<Result<Vec<_>, _>>()?,
            aerial_color_back: settings.aerial_color_back.iter().map(ColorCurveEntry::try_from).collect::<Result<Vec<_>, _>>()?,
            aerial_final_exp: settings.aerial_final_exp.iter().map(ScalarCurveEntry::try_from).collect::<Result<Vec<_>, _>>()?,
        })
    }
}
// ----------------------------------------------------------------------------
impl<'a> TryFrom<&'a ColorEntry> for ColorCurveEntry {
    type Error = String;

    fn try_from(value: &ColorEntry) -> Result<Self, Self::Error> {
        ColorCurveEntry::try_from((
            value.time(),
            value.color().0,
            value.color().1,
            value.color().2,
            value.color().3,
        ))
    }
}
// ----------------------------------------------------------------------------
impl<'a> TryFrom<&'a ScalarEntry> for ScalarCurveEntry {
    type Error = String;

    fn try_from(value: &ScalarEntry) -> Result<Self, Self::Error> {
        ScalarCurveEntry::try_from((value.time(), value.value()))
    }
}
// ----------------------------------------------------------------------------
